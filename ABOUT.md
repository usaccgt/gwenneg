# À propos

Ce simulateur a pour objectif de permettre aux agents de la DGAC de calculer leur salaire théorique de la façon la plus précise possible, en fonction des textes applicables.

Le code source est disponible sur [gitlab.com](https://gitlab.com/usaccgt/gwenneg) sous licence [AGPL-V3.0](https://www.gnu.org/licenses/agpl-3.0.en.html#license-text).

Il est dérivé du simulateur [simupaye](https://github.com/dgac/simupaye/") créé par Bruno Spyckerelle.

**Contributeurs projet original :**

- Bruno Spyckerelle
- Nathalie Spyckerelle
- Jérôme Caffin

**Contributeurs USACcgt :**

- Fabien André, Rennes
- Gilles Cambefort, Organisme de Contrôle de Bâle-Mulhouse
- Hervé Sibille, Reims
- Samuel Gonny, Fort-de-France
- Jonathan Muchler, Orly

# Dernières modifications

**0.12.0 (23 février 2025)**

- Ajout des primes attractivités et fidélisation pour les TSEEAC, ICNA et IEEAC
- Ajout de la prime encadrement DO
- Correction part technique TSEEAC sur poste CTACable
- Ajout indemnité d'astreinte
- Autres corrections
  - Correction du statut siege de Saint-Denis de la Réunion en siège SNA
  - Limitation annuelle du forfait télétravail

**0.11.1 (5 février 2025)**

- prise en compte des changements du 1er janvier 2025 (ISQ, part technique TS)

**0.11.0 (5 février 2025)**

- Mise à jour part technique TSEEAC
- Suggestion de fonction pour les encadrants
- Maintien d'ISQ pour les ICNA de l'ENAC, DTI ou DO
- Modifications de l'affichage sur mobile

**0.10.1 (20 janvier 2025)**

- Ajout de la prime première affectation du protocole 2023-2027.
- Mise en forme des fonctions par niveau

**0.9.2 (8 janvier 2025)**

- Mise à jour ISQ du protocole 2023-2027.
- Ajout du niveau de réorganisation Projet de service spécifique
- Gestion des différents part qualification pour les TSEEAC (contrôleur ou non)
- Suggestion de fonction pour les TSEEAC
- Ajout prime CDG pour les TSEEAC
