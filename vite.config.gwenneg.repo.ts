import viteConfig from './vite.config';
import { mergeConfig, defineConfig } from 'vitest/config'


export default defineConfig(configEnv => mergeConfig(
    viteConfig(configEnv),
    defineConfig({
        base: '/gwenneg/',
    })
  ))