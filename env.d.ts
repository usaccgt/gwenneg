/// <reference types="vite/client" />

// Global constant replacements https://vitejs.dev/config/shared-options.html#define
declare const __BUILD_DATE__: string;

declare const __VUE_APP_GIT_HASH__: string;
declare const __VUE_APP_SEMVER_STRING__: string;

declare module '*.md' {
  // "unknown" would be more detailed depends on how you structure frontmatter
  const attributes: Record<string, unknown>;

  // When "Mode.HTML" is requested
  const html: string;

  // Modify below per your usage
  export { attributes, html };
}
