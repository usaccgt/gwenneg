# Gwenneg
***Simulateur de salaire fonction publique par [l'USACcgt](https://www.usac-cgt.org/)***

D'après [DGAC/simupaye](https://github.com/dgac/simupaye/) de Bruno Spyckerelle. (Largement modifié depuis)

Disponible sous [licence AGPL-3.0](LICENSE)

[Doc. Developeurs](doc/dev.md) | [À propos de l'appli](ABOUT.md) | [Signaler une erreur](CONTRIBUTING.md)

## Nouvelle organisation du dépot (novembre 2024)

Le backend (calcul de la feuille de paye) va être réusiné en typescript de manière à apporter une meilleur maintenabilité. Le but est de préparer le code à supporter d'autres corps. On en a profitté pour réorganiser le dépot et mettre à jours les librairies : 

* Passage à Vuejs 3.5 (avec le support de typescript activé), Cypress 13, Eslint 9...
* Utilisation de Vite à la place de vue-cli (moteur de dev et de build)
* Suppression de l'ancien front jekyll non maintenu.
* Suppression du support des très vieux navigateurs. (Retrait de babel)
* Supression de librairies obsolètes
* Déplacements/renommage de certain répertoire

[Documentation sur les commandes npm & l'organisation de la CI](doc/npm_ci.md)

### Les répertoires

Vue est maintenant installé à la racine du dépot. (avant il était installé dans vue/). Avec vite, le fichier index.html doit se trouver à la racine.

* `bin/` : scripts et binaires de gestion. Par exemple c'est qu'ici que ce trouve les scripts qui transforme les datas csv en fichier javascript utilisé par l'outil. (anciennement : scripts)
* `datas/` : Les différentes données de configuration (grille, liste des sites, ...)
* `public/` : ces fichiers seront transférés tel quel dans le code compilé.
* `src/frontend/` : Le code de l'apparence (frontend/vue)
* `src/old_js/` : L'ancien code du calcul de la feuille de paye. (auparavant dans /js/)
* `tests/cypress/` : Les tests cypress (end 2 end)
* `tests/core/` : Les anciens tests du calcul de la feuille de paye (jest.)
* `tests/unit/` : Les tests unitaires (Vitest). example.test.js est l'exemple de base du framework.

## Utilisation

Rendez-vous sur http://simulateur.usac-cgt.org/

## Aide à l'amélioration de l'outil

Merci d'avance pour votre implication. Pour participer : 

* créer une bifurcation du dépot (fork)
* Faire vos modifications (voir plus bas)
* Créer une requète de fusion

### Mise à jours des données de configuration

Si vous voulez simplement mettre à jours les donnneés de configuration (dans datas/), le plus simple est d'utiliser les outils d'édition de GitLab. Cependant, ce mode de fonctionnement ne vous permetteras pas de tester vos modifications.

### Avec Gwenneg-IDE

Gwenneg-IDE est une Machine Virtuelle s'installant facilement avec un IDE inclus (VSCode) et un accès par navigateur. Est notamment inclus les outils de dev permetterant de voir vos modifications.

[Voir le dépot de Gwenneg IDE](https://gitlab.com/JiCambe/gwenneg-ide)


### Sur Votre machine

1. Node JS 20 doit être installé sur votre machine
2. cloner (git clone) le code source de votre bifurcation (fork). Vous pouvez utiliser [Ungit](https://github.com/FredrikNoren/ungit) si vous ne voulez pas passer par la ligne de commande pour les opérations git
3. Installer les librairies : 
```bash
cd {REPO_DIRECTORY}/tests/core
npm install
npm run postinstall
cd {REPO_DIRECTORY}
npm install
cd {REPO_DIRECTORY}/src/old_js
npm install
```

#### Pour Lancer Vue en mode debug

```bash
cd {REPO_DIRECTORY}
npm run dev
```

#### Pour compiler l'application

```bash
cd {REPO_DIRECTORY}
npm run build
```

#### Pour tester la compilation précédente

```bash
cd {REPO_DIRECTORY}
npm run preview
```
