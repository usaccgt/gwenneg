function expectItemSocialToBe(itemSocial, amount, waitAmount, delta) {
  /*
    use .should('include.text',...) to wait for propagation
    then dive in DOM to perform string equality text.
    */
  waitAmount ??= Math.floor(amount);
  delta ??= 0.01;
  return itemSocial
    .find('.item_social_valeur, .item_social_part_salariale > span')
    .should('include.text', `${waitAmount}`)
    .should(($p) => expect(parseFloat($p[0].getAttribute('data-value'))).to.be.closeTo(amount, delta));
}

/* copied from Arnaud Peloquin answer https://stackoverflow.com/a/52219275/
  sCC-BY-SA 4.0 */
Cypress.Commands.add('selectContaining', {prevSubject: 'element'}, (subject, text, options) => {
  return cy.wrap(subject).contains('option', text, options).then(
    option => cy.get(subject).select(option.text().trim())
  );
});

export {
  expectItemSocialToBe,
};
