// https://docs.cypress.io/api/table-of-contents

describe('Homepage displays', () => {
  it('Visits the app root url', () => {
    cy.visit('/');
    cy.contains('h1', 'Salaire');
  });
});

describe('Modal map', () => {
  it('open and close', () => {
    const width = 1920;
    cy.viewport(width, 1080);

    cy.visit('/');

    cy.get('#metroMap').invoke('outerWidth').should('be.lt', width / 3);
    cy.get('#guyaneMap').should('not.be.visible');
    cy.get('button.modal-switch').click();
    cy.get('#metroMap').invoke('outerWidth').should('be.gt', 0.95 * width);
    cy.get('#guyaneMap').should('be.visible');
    cy.get('button.modal-switch').click({ force: true });
    cy.get('#metroMap').invoke('outerWidth').should('be.lt', width / 3);
  });
});

function testNavLink(label, targetURL) {
  cy.visit('/');
  let link = cy.get('nav > a').contains(label);
  link.should('be.visible');
  link.click();
  cy.url().should('eq', Cypress.config().baseUrl + targetURL)
}

describe('About page', () => {
  it('displays about link', () => testNavLink('À propos', '/about'));
  it('contains stuff', () => {
   // FIXME should be unit test
    cy.visit('/about');
    cy.get('h1'); // attends affichage contenu
    let content = cy.get('header').next();
    content.get('p').first().should('contain', 'simulateur');
    content.get('a').contains('AGPL').should('be.visible');
  });
});

describe('Contributing page', () => {
  it('displays link', () => testNavLink('problème', '/contributing'));
  it('contains stuff', () => {
    // FIXME should be unit test
    cy.visit('/contributing');
    cy.get('h1'); // attends affichage contenu
    let content = cy.get('header').next('div');
    cy.log(content);
    cy.get('.contributing img').first().should('have.attr', 'src').should('include','declarer_bug_1.jpg');
    cy.get('.contributing img').first().invoke('outerWidth').should('be.gt', 100);
    cy.get('.contributing img').first().next().invoke('outerWidth').should('be.gt', 100);
  });
});
