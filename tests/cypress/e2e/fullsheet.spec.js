/* regular import:
    import 'cypress-get-by-label/commands';
  does not allow to pass subject to getByLabel */
import { registerCommand } from 'cypress-get-by-label/src/';
registerCommand ();

// side effect: add selectContaining command
import { expectItemSocialToBe } from './common.js';

describe('Testeur 1', () => {
  it('performs IESSA simulation', () => {
    cy.visit('/');
    const store = cy.window().its('store');
    store.invoke('dispatch', 'setDate', '01/07/2023');

    cy.get('li.gtab-button[data-tab="social"]').click();

    expectItemSocialCodeToBe('201987', 0);
    cy.get('#sitePicker1>select').selectContaining('Paris - Orly');
    expectItemSocialCodeToBe('201987', 160.88);

    // cypress.click() broken by animation, rely on DOM.click
    cy.get('.divisionnaire').contains('2')
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      }).click();

    expectItemSocialCodeToBe(101000, 2126.64);
    expectItemSocialCodeToBe(101050, 236.06);
    expectItemSocialCodeToBe(102000, 63.80);

    cy.get('#carriere-iessa').getByLabel('Part Technique').select('Titulaires de la QTS et service réorganisé');
    cy.get('div.fonctions-evs button').contains("GRT").click();

    cy.getByLabel('NBI').check();

    expectItemSocialCodeToBe('102000', 74.88);
    cy.get('span:contains("Abonnement transport")').click();
    // cy.get('button:contains("navigo")').click(); // has been updated
    cy.get('#inputAbo').type('77.08{enter}');

    cy.getByLabel('Compensation CSG').type('95.86{enter}');
    cy.getByLabel('Taux imposition').type('15.8{enter}');
    cy.getByLabel('Jours télétravail').type('3{enter}');

    expectItemSocialToBe(cy.get('[data-cy="netapayer"]'), 4099.74);
  });
});

describe('Testeur 2', () => {
  it('performs TSEEAC simulation', () => {
    cy.visit('/');
    const store = cy.window().its('store');
    store.invoke('dispatch', 'setDate', '01/12/2022');

    cy.get('#corpsButton-tseeac').click();

    cy.get('#sitePicker1>select').selectContaining('Châlons-Vatry');
    cy.wait(0);
    cy.get('.principal').contains('4')
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      }).click();

    cy.get('button').contains('< 4 ans').click();

    cy.get('.expand').click({ force: true });

    cy.getByLabel('Niveau').clear().type('9{enter}');
    cy.get('label:contains("NBI")').first().click();

    // cy.getByLabel('Enfants à charge').type('{upArrow}{upArrow}');
    cy.getByLabel('Enfants à charge').type('2{enter}');
    cy.getByLabel('Compensation CSG').type('56.21{enter}');
    cy.getByLabel('Taux imposition').type('9.9{enter}');

    expectItemSocialToBe(cy.get('[data-cy="netapayer"]'), 4703.98);
  });
});

describe('Testeur 5', () => {
  it('IESSA La Réunion', () => {
    cy.visit('/');
    const store = cy.window().its('store');
    store.invoke('dispatch', 'setDate', '01/01/2024');
    cy.get('#sitePicker1>select').selectContaining('Saint Denis La Réunion');
    cy.wait(0);
    cy.get('.divisionnaire').contains('9')
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      }).click();

    cy.get('#carriere-iessa').getByLabel('Part Technique')
      .select('Titulaires de la QTS + 10 ans et service réorganisé');
    cy.get('div.fonctions-evs button:contains("Expert Conf")').click();

    cy.getByLabel('NBI').check();

    cy.getByLabel('Enfants à charge').type('3{enter}');

    cy.getByLabel('Compensation CSG').type('72.67{enter}');
    cy.get('label:contains("Participation mutuelle")').first().click();

    expectItemSocialToBe(cy.get('[data-cy="netapayer"]'), 8207.92 + 9.60, undefined, 0.5); // ticket restau non gérés
  });
});

function getItemSocialFromCode(c) {
  // assume code is in description
  return cy.get('p.item_social_description').contains(c).parentsUntil('div.section').filter('div.item_social');
}

function expectItemSocialCodeToBe(c, amount) {
  return expectItemSocialToBe(getItemSocialFromCode(c), amount);
}
