import 'cypress-get-by-label/commands'; // add cy.getByLabel

// side effect: add selectContaining command
import { expectItemSocialToBe } from './common.js';

describe('Testeur 3', () => {
  it('ICNA D8 Athis', () => {
    cy.visit('/');
    const store = cy.window().its('store');
    store.invoke('dispatch', 'setDate', '01/03/2024');

    cy.get('#corpsButton-icna').click();

    cy.get('#sitePicker1>select').selectContaining('Athis-Mons CRNA-Nord');
    cy.wait(0);
    cy.get('.divisionnaire').contains('8')
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      }).click();

    cy.getByLabel('NBI').check();

    cy.get('button:contains("PC")').click();
    cy.getByLabel('Enfants à charge').type('1{enter}');

    // cy.getByLabel('Enfants à charge').type('{upArrow}{upArrow}');
    cy.getByLabel('Compensation CSG').type('70.78{enter}');
    cy.get('label:contains("Participation mutuelle")').first().click();

    cy.getByLabel('Taux imposition').type('14.8{enter}');

    expectItemSocialToBe(cy.get('[data-cy="netapayer"]'), 6959.80, undefined, 0.5);
  });
});

describe('Testeur 4', () => {
  it('ICNA Chef2 CRNA-O', () => {
    cy.visit('/');
    const store = cy.window().its('store');
    store.invoke('dispatch', 'setDate', '01/12/2023');

    cy.get('#corpsButton-icna').click();

    cy.get('#sitePicker1>select').selectContaining('Brest CRNA-Ouest');
    cy.wait(0);
    cy.get('.chef').contains('2')
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      }).click();

    cy.getByLabel('Enfants à charge').type('2{enter}');
    cy.get('button:contains("PC")').click();

    cy.getByLabel('Compensation CSG').type('67.74{enter}');
    cy.get('label:contains("Participation mutuelle")').first().click();

    cy.getByLabel('Taux imposition').type('14.5{enter}');

    expectItemSocialToBe(cy.get('[data-cy="netapayer"]'), 6734.87, undefined, 0.5);
  });
});
