import {jest} from '@jest/globals';

import { Fonctions, getLikelyFonction} from '../../src/old_js/evs';
import { getSiteFromId } from "../../src/old_js/sites.js";

describe("Simple fonction", () => {
    test('complete (IESSAAffecté)', () => {
      Fonctions
      let f = Fonctions.IESSAAffecté;
      expect(f.key).toBe('IESSAAffecté')
      expect(f.label.length).toBeGreaterThan(3);
      expect(f.desc.length).toBeGreaterThan(f.label.length);
      expect(f.niveau).toBe(1);
    })
    test('sparse (DMS)', () => {
        // label = key if not specified
        let f = Fonctions.DMS;
        expect(f.key).toBe('DMS')
        expect(f.label).toBe('DMS')
    })
});

describe("Callable fonction", () => {
    test('expert conf', () => {
      let f = Fonctions.ExpertConf(getSiteFromId("LFFF"));
      expect(f.key).toBe('ExpertConf')
      expect(f.niveau).toBe(11);

      let f2 = Fonctions.ExpertConf(getSiteFromId("LFRN"))
      expect(f2.key).toBe('ExpertConf')
      expect(f2.niveau).toBe(10);
    })
});

describe("Some getLikely common case", () => {
  [
    ['PC CRNA',
      ["icna", getSiteFromId("LFEE"), "divisionnaire", 4],
      ["PC", "CDS"]
    ],
    ['PC CDG', ["icna", getSiteFromId("LFPG"), "divisionnaire", 4]],
    ['ICNA Normal', ["icna", getSiteFromId("LFPG"), "normal", 2]],
    ['ICNA Enac',
      ["icna", getSiteFromId("ENAC"), "divisionnaire", 2],
      ["ICA"]
    ],
    ['Ts AD',
      ["tseeac", getSiteFromId("LFPN"), "normal", 4],
      ["ATC d'AD"],
    ],
    ['Ts Cdg',
      ["tseeac", getSiteFromId("LFPG"), "normal", 4],
      ["VT"],
    ],
    ['Ts Orly',
      ["tseeac", getSiteFromId("LFPO"), "normal", 4],
      ["PCNA"],
    ],
    ['Ts CRNA',
      ["tseeac", getSiteFromId("LFFF"), "normal", 4],
      ["BTIV"],
    ],
    ['IESSA encadrant DTI',
      ['IESSA', getSiteFromId("DTI"), "divisionnaire", 6, 1],
      ["Adj. de pôle"],
    ],
  ].forEach(testDesc => {
    test(testDesc[0], () => {
      const consoleSpy = jest.spyOn(console, 'warn')
      const fonctions = getLikelyFonction(...testDesc[1]);
      expect(consoleSpy).toHaveBeenCalledTimes(0);
      const labels = fonctions.map(f =>f.label)
      if(testDesc[2]) {
        testDesc[2].forEach(expectedLabel =>
          expect(labels).toContain(expectedLabel)
        )
      }
    })
  })
});

describe("avoid regression in CTACable deducer", () => {
  let shouldExists = [
    'ChefPole','ChefSub', 'ExpertSenior','AssSub',
    'ExpertConf',
    'IE', 'EnsConf', 'EnsSenior',
  ];
  shouldExists.forEach((f) => {
    expect(Fonctions).toHaveProperty(f)
  });
});
