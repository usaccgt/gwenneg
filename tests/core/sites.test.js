


import { getSitesFromCorps,getSiteFromId } from "../../src/old_js/sites.js";

describe("Gestion corps", () => {
    test('iessa : orly, toussus', () => {
      let sites = getSitesFromCorps('iessa')
      expect (sites).toContain(getSiteFromId('LFPO'))
      expect (sites).toContain(getSiteFromId('LFPN'))
      expect (sites).not.toContain(getSiteFromId('LFPM'))
    })
    test('icna : orly, melun', () => {
        let sites = getSitesFromCorps('icna')
        expect (sites).toContain(getSiteFromId('LFPO'))
        expect (sites).toContain(getSiteFromId('LFPM'))
        expect (sites).not.toContain(getSiteFromId('LFPN'))
      })
})

describe("transport", () => {
  test('simple', () => {
    let t = getSiteFromId('LFPN')
    expect (t).toHaveProperty('transport')
    expect (t.transport).toHaveProperty('navigo');
    let navigoMensuel = 88.8; // valeur au 1/01/2025
    let navigoReduit = 11 * navigoMensuel / 12;
    expect (t.transport.navigo).toBeCloseTo(navigoReduit)
  })
  test('multiple', () => {
      let o = getSiteFromId('LFPO')
      expect (o).toHaveProperty('transport')
      expect (o.transport).toHaveProperty('navigo')
      expect (o.transport).toHaveProperty('orlyval')
    })
})
