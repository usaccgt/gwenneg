

import { DataProviderFactory } from "../../src/old_js/data.js";


const dp = DataProviderFactory.getInstance();

describe("Update Prime 2023", () => {
    test('iessa PartExp', () => {
      dp.setDate("01/01/2023");
      expect (dp.partTechIESSA[3]).toBeCloseTo(770.23, 2);
    })
})

describe("Update indice 1er juillet 2023", () => {
  test('simple', () => {
    dp.setDate("01/07/2023");
    expect(dp.point_indice).toBeCloseTo(4.92,2)
  })
})

describe("data set date compare", () => {
  test('generic before', () => {
  });
  test('generic after', () => {
    dp.setDate("01/01/2024")
    expect(dp.isAfter("01/01/2024")).toBe(true);
  });
  test('beforeProtocole', () => {
    dp.setDate("30/06/2024")
    expect(dp.isBeforeProtocole2023()).toBe(true);
    dp.setDate("01/07/2024")
    expect(dp.isBeforeProtocole2023()).toBe(false);
  });
  test('afterProtocole', () => {
    dp.setDate("30/06/2024")
    expect(dp.isAfterProtocole2023()).toBe(false);
    dp.setDate("01/07/2024")
    expect(dp.isAfterProtocole2023()).toBe(true);
  });
});

describe("import yaml data", () => {
  test('fonction', () => {
    dp.setDate("01/07/2017");
    expect(Object.keys(dp.fonctions)).toContain("IESSAAffecté");
    dp.setDate("01/07/2024");
    expect(Object.keys(dp.fonctions)).toContain("IESSAAffecté");
  });
});