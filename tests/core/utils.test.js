

import {
    compareFrenchDateStr,
    frenchDatestrToDate,
    isValidFrenchdateStr,
 } from "../../src/old_js/utils.js";

describe("parse french date", () => {
    test('Juin 2022', () => {
        let d = frenchDatestrToDate("01/06/2022");
        expect(d.getUTCFullYear()).toBe(2022);
        expect(d.getUTCMonth()).toBe(5);
        expect(d.getUTCDate()).toBe(1);
    })
});

describe("validity check of french date", () => {
    test('correct', () => {
        expect(isValidFrenchdateStr("01/07/2023")).toBe(true);
        expect(isValidFrenchdateStr("31/12/2023")).toBe(true);
    });
    test('incorrect', () => {
        expect(isValidFrenchdateStr("1/7/2023")).toBe(false);
        expect(isValidFrenchdateStr("12/31/2023")).toBe(false);
        expect(isValidFrenchdateStr("01/01/24")).toBe(false);
    });
});

describe("compare french date", () => {
    test('normal', () => {
        expect(compareFrenchDateStr("01/07/2024", "04/07/2024")).toBe(3);
        expect(compareFrenchDateStr("01/01/2024", "01/01/2024")).toBe(0);
        expect(compareFrenchDateStr("30/06/2024", "01/07/2024")).toBe(1);
    });
});
