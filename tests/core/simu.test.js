
/**
 * How to run test:
 *  cd tests/core
 *  npm install
 *  npm test
 *
 * or package script:
 *   npm run test:core
 */

import debugModule from 'debug';
const debug = debugModule("gwenneg:test:core");

import "./simu.js";

import { PaySheetComputer } from "../../src/old_js/core.js";
import { DataProviderFactory } from "../../src/old_js/data.js";

let dp = DataProviderFactory.getInstance("01/01/2023");
let c = new PaySheetComputer(dp);

function doOneTest(input, total, netir) {
  let paysheet = compute(input);
  debug(`total : ${paysheet.total}  expect: ${total}`)
  try {
    let delta = Math.abs(paysheet.total - total)
    expect (delta).toBeLessThan(10);
    if (netir) {
      debug(`netir : ${paysheet.netir}  expect: ${netir}`);
      delta = Math.abs(paysheet.netir - netir);
      expect (delta).toBeLessThan(10);
    }
  } catch (e) {
    console.debug("Input:\n",input,"\nPaysheet:\n",paysheet)
    throw e;
  }
}

function compute(input) {
  dp.setDate(input.validity)
  return c.compute_income(input);
}

describe("Tester 1", () => {
    test('Juin 2022', () => {
      let input = {
        "corps": "iessa",
        "grade": "normal",
        "echelon": 3,
        "nbi": "0",
        "evs": "10",
        "region": "3",
        "peq": "3",
        "reorg": "service",
        "pcs": "150",
        "rembt": "34.46",
        "indem_csg": "87.11",
        "tauxIR": "15.8",
        "validity": "01/01/2020"
      };
      let total = 4323.61
      doOneTest(input, total);
    });
  test("Décembre 2022", () => {
    let input = {
      "corps": "iessa",
      "grade": "normal",
      "echelon": 4,
      "nbi": "1",
      "evs": "10",
      "region": "3",
      "peq": "3",
      "reorg": "service",
      "pcs": "150",
      "rembt": "34.46",
      "indem_csg": "87.11",
      "tauxIR": "15.8",
      "nuit": "3",
      "validity": "01/07/2022"
      };
    let total = 4801.51;
    doOneTest(input, total);
  });
  test("Août 2023", () => {
    let input =  {
      "corps": "iessa",
      "validity": "01/07/2023",
      "grade": "divisionnaire",
      "echelon": 2,
      "pcs": 150,
      "region": "3",
      "reorg": "service",
      "peq": 3,
      "evs": 10,
      "nbi": 1,
      "rembt": 38.54,
      "indem_csg": "95.86",
      "tauxIR": "15.8",
      "forfaitTeletravail": "3"
    };
    let total = 4890.80;
    let netir = 4099.74;
    doOneTest(input, total, netir);
  });
})

describe("Tester 2", () => {
  test('Décembre 2022', () => {
    let input = {
    "corps": "tseeac",
    "validity": "01/07/2022",
    "grade": "principal",
    "echelon": "4",
    "nbi": "1",
    "evs": "9",
    "region": "1",
    "famille": "2",
    "TSEtape": 5,
    "part3": "autre",
    "tsgroupe": "G",
    "licence": 101,
    "pcs": "150",
    "indem_csg": "47.81"
    };
    let total = 5232.44
    doOneTest(input, total);
  })
})

describe("Tester 3", () => {
  test('Mai 2023', () => {
    let input = {
      "corps": "icna",
      "validity": "01/01/2023",
      "xpRH": 1,
      "xpRHOption": "1bis",
      "xpRHGroupe": "0",
      "grade": "divisionnaire",
      "xpRHCyclesRH": "3",
      "groupe": "A1",
      "echelon": "10",
      "evs": "10",
      "licence": "5",
      "region": "1",
      "nbi": "1",
      "indem_csg": "63.79",
      "tauxIR": "20",
      "rembt": "34.47",
      "psc": 1
    };
    let total = 7432.56
    doOneTest(input, total);
  })
})

describe("prime CDG IESSA", () => {
  test("RIST 2017", () => {
    let input1 = {
      corps: "iessa",
      evs: 5,
      validity: "01/01/2020",
    }
    let sans = compute(input1)
    let input2 = {...input1}
    input2.majPFRistCDG = 1;
    let avec = compute(input2);
    let delta = avec.total_pos - sans.total_pos;
    expect(delta).toBeCloseTo(120);
  })
  test("RIST 2023", () => {
    let input1 = {
      corps: "iessa",
      evs: 5,
      validity: "01/01/2023",
    }
    let sans = compute(input1)
    let input2 = {...input1}
    input2.majPFRistCDG = 1;
    let avec = compute(input2);
    let delta = avec.total_pos - sans.total_pos;
    expect(delta).toBeCloseTo(124.2);
  })
})

describe("outremer", () => {
  test("martinique", () => {
    let input1 = {
      corps: "iessa",
      grade: "divisionnaire",
      echelon: "9",
      validity: "01/07/2022",
    }
    let sans = compute(input1)
    let input2 = {...input1}
    input2.majDOMTOM = "mq";
    let avec = compute(input2);
    let delta = avec.total_pos - sans.total_pos;
    expect(delta).toBeCloseTo(1251.30);
  })
})

describe("indice", () => {
  test("ICNA 2020", () => {
    let input = {
      corps: 'icna',
      grade: 'divisionnaire',
      echelon: 10,
      validity: '01/01/2020',
    }
    let paysheet = compute(input);
    expect(paysheet.indice).toBe(685);
  })
})
