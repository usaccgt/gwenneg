// See : https://vitest.dev/guide/
// See : https://vitest.dev/guide/migration.html#migrating-from-jest
import { shallowMount } from '@vue/test-utils';
import { expect, describe, it } from 'vitest'
import ToggleButton from '@/components/input/widgets_general/ToggleButton.vue';

describe('ToggleButton.vue', () => {
  it('renders label when passed', () => {
    const msg = 'Hello';
    const wrapper = shallowMount(ToggleButton, {
      props: { label: msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
