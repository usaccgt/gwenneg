// See : https://vitest.dev/guide/
// See : https://vitest.dev/guide/migration.html#migrating-from-jest

import { createApp } from 'vue'
import { expect, describe, it } from 'vitest'

import App from '@/App.vue'
import store from '@/store'
import router from '@/router'
import { DataProviderFactory } from '@/../../src/old_js/data';


const app = createApp(App).use(store).use(router);
const dp = DataProviderFactory.getInstance();

describe('Encadrement DO', () => {
  it('works for CDT', () => {
    const { input } = store.state;
    Object.assign(input, {
        site: "LFPO",
        corps: "icna",
        fonction: "CDT",
    })

    expect(store.getters.paysheet.lines).toHaveProperty(202548)
    expect(store.getters.paysheet.lines[202548].amount).toBe(dp.primeEncadrementDO[5]);
  });
  it('is present only after procotole2023', () => {
    /* warning: state is shared between tests */
    const { input } = store.state;
    Object.assign(input, {
        validity: "30/06/2024",
        // site: "LFPO",
        // corps: "icna",
        // fonction: "CDT",
    })
    expect(store.getters.paysheet.lines).toEqual(expect.not.objectContaining({202548: expect.anything()}))
  })
});
