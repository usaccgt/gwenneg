/**
 * Met à jour le champs citycode et le champs mobilité (taux du versement mobilité employeur)
 * Télécharge le fichier avec les taux sur le site de l'URSAFF
 * Récupère les adresse depuis coordonées géographique (API Adresse)
 *
 * lancer depuis la racine du projet :
 *       node bin/mobilite/update.js
 * (ne gère pas les chemins relatifs)
 *
 */

import fs from "fs";
import path from "path";
import { fileURLToPath } from 'url';

import download from "download";
import fetch from "node-fetch";
import Papa from "papaparse";

function getDateYYYYMMDD() {
  const a = new Date()
  return a.getUTCFullYear()
   +(a.getUTCMonth()+1).toString().padStart(2,0)
   + a.getUTCDate().toString().padStart(2,0);
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let siteFile = './datas/csv/sites.csv';

class TauxTransport {
  constructor () {
    this.tauxTransportFile = `tauxTransport.${getDateYYYYMMDD()}.csv`
    this.tauxTransportPath = path.join(
      __dirname,
      this.tauxTransportFile
    )
    // https://fichierdirect.declaration.urssaf.fr/static/tauxTransport.20231015.csv
    this.filePromise = this.dowloadIfNeeded().then(()=>this.readAndParse());
  }

  async dowloadIfNeeded() {
    if (! fs.existsSync(this.tauxTransportPath)) {
      let url = `https://fichierdirect.declaration.urssaf.fr/static/${this.tauxTransportFile}`;
      console.log(url);
      await download(url, __dirname);
      if (! fs.existsSync(this.tauxTransportPath)) {
        console.error(`par réussi à télécharger ${tauxTransportFile}\ndepuis ${url}`);
        process.exit();
      }
    }
    return this.tauxTransportPath;
  }
  readAndParse() {
    let file = Papa.parse(
      fs.readFileSync(this.tauxTransportPath).toString(),
      {
        header: true,
      }
    );
    return Promise.resolve(file);
  }
  async getTauxFromCityCode(citycode) {
    const citycodeWithQuote = `'${citycode}'`;
    this.file = await this.filePromise;
    let lines = this.file.data.filter(l=>l["Code commune"] == citycodeWithQuote)
    if (lines.length > 0) {
      let tauxAOT = 0;
      let tauxSM = 0;
      for (let line of lines) {
        if (line["Taux AOT"] != "0") {
          tauxAOT=parseFloat(line["Taux AOT"])
        }
        if (line["Taux syndicat mixte"] != "0") {
          tauxSM=parseFloat(line["Taux syndicat mixte"])
        }
      }
      return Math.round((tauxAOT + tauxSM)*100)/100;
    } else {
      return undefined;
    }
  }
}

async function updateFile(filename, tauxTransportProvider) {
  let parsed = Papa.parse(
    fs.readFileSync(filename).toString(),
    {
      header: true,
      skipEmptyLines: true,
    }
  );

  let i = 0;
  for (let line of parsed.data) {
    console.log(`${line.nom}\nlon:${line.lon}, lat:${line.lat}`);
    let citycode = line.citycode;
    if (!citycode) {
      try {
        let adresse = await getAdresse(line);
        // console.log(adresse);
        console.log(`${adresse.city} (${adresse.citycode})`);
        citycode = adresse.citycode;
        line.citycode = citycode;
      } catch (e) {
        console.log(e);
      }
    }
    if (citycode) {
      let taux = await tauxTransportProvider.getTauxFromCityCode(citycode);
      console.log(taux);
      if (taux && taux != line.mobilite) {
        console.log(`update taux ${line.mobilite} => ${taux}`)
        line.mobilite = taux;
      }
    }
  }
  let content = Papa.unparse(parsed);
  return fs.writeFile(filename, content, (err) => {
      if (err) throw err;
    });
}

function getAdresse(aSite) {
  const lon = aSite.lon > 180 ? (aSite.lon - 360): aSite.lon;
  const url = `https://api-adresse.data.gouv.fr/reverse/?lon=${lon}&lat=${aSite.lat}`;
  return new Promise((resolve, reject) => {
    fetch(url).then((response) => {
      if (response.ok) {
        const contentType = response.headers.get('Content-Type') || '';
        if (contentType.includes('application/json')) {
          response.json().then((obj) => {
            if (obj.features.length > 0) {
              resolve(obj.features[0].properties);
            } else {
              reject(new Error('no result'));
            }
          }, (error) => {
            reject(new Error(`Invalid JSON: ${error.message}`));
          });
        } else {
          reject(response);
        }
      } else {
        reject(response);
      }
    });
  });
}


updateFile(siteFile, new TauxTransport());
// console.log(new TauxTransport().getTauxFromCityCode(34154));