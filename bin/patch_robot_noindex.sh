#! /bin/bash

xmlstarlet ed --inplace \
    -s //html/head -t elem -n meta \
    --var new-field '$prev' \
    -i '$new-field' -t attr -n name -v robot \
    -i '$new-field' -t attr -n content -v noindex \
    index.html
