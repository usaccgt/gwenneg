/**
 * Génère des fichiers CSV depuis fichiers javascript avec variables
 * globales.
 * Lancer depuis la racine du projet :
 *       node bin/csv/export.js
 *
 * (ne gère pas les chemins relatifs)
 */

import common from "./common.js"

var datadir = '../../src/old_js/datas';
var datadir2 = './datas';

let includes = [
  datadir + "/const.js",
  datadir + "/grades.js",
  datadir + "/grilles.js",
];

for (const filename of includes) {
  //(1, eval) is required to evaluate in global namespace
     /* does not work with modules */
    /* (1, eval)(fs.readFileSync(filename).toString()); */
    await common.registerModule(filename);
};

(1, eval)("var generated = import('../../src/old_js/datas/generated.js')")

// scheme = {
//   point_indice,
//   rpc_rate,
//   activity_rate,
//   pcs,
//   points_nbi,
//   date,
//   transfertRetenue,
//   csg_deduc,
//   modulationRMA,
//   table_indices,
//   fonctions,
//   grades,
//   echelons,
//   partTechIESSA,
//   peqLevel,
//   sft_fixe,
//   sft_prop,
// };

let scheme = common.scheme;

scheme.forEach(item => {
    item.desc.exportCSV(datadir2+"/csv")
})
