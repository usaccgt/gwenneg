

import fs from "fs";
import Papa from "papaparse";


function mkdir_p(dir) {
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir, { recursive: true });
    }
}

const modules = {};
const keywords = new Set(['const']);

async function registerModule(filename) {
    let basename = filename.split('/').reverse()[0];
    let varname = basename.split('.').slice(0,-1).join('');
    if (keywords.has(varname)) {
        varname = "_"+varname;
    }
    modules[varname] = await import(filename);
}

class Data {
    constructor(name, fields, varname) {
        this.name = name
        this.parseParentDir()
        this.fields = fields
        this.varname = varname
    }

    parseParentDir() {
        if (this.name.includes('/')) {
            let tokens = this.name.split('/')
            this.parentDirs = tokens.slice(0, -1)
            this.name = tokens[tokens.length-1]
        } else {
            this.parentDirs = []
        }
    }
    getDirStack(dir) {
        return [dir].concat(this.parentDirs)
    }
    getDir(dir) {
        return this.getDirStack(dir).join("/")
    }

    async getObj(o) {
        let obj;
        if (this.varname) {
            for(var k of Object.keys(modules)) {
                if (this.varname in await modules[k]) {
                    obj = modules[k][this.varname];
                    break;
                }
            }
            let generated = await (1, eval)(`generated`);
            obj = generated[this.varname];
        } else {
            obj = o;
        }
        return obj;
    }
}

class TabularData extends Data {
    constructor(name, fields, varname) {
        super(name, fields, varname)
    }
    async exportCSV (dir, o) {
        let obj = await this.getObj(o)
        let fulldir = this.getDir(dir)
        mkdir_p(fulldir)
        let filename = [fulldir, this.name].join("/") + ".csv";
        let content = Papa.unparse(obj, {columns: this.fields })
        return fs.writeFile(filename, content, (err) => {
            if (err) throw err;
          });
    }
    parseCSV(dir) {
        let filename = dir + "/" + this.name + ".csv";
        let parsed = Papa.parse(
            fs.readFileSync(filename).toString(),
            {
                header: true,
                columns: this.fields
            });
        return parsed.data;
    }

}

export class HierarchicalData extends Data {
    constructor(name,fields,csvFieldsNumber, varname) {
        super(name, fields, varname)
        this.csvFieldsNumber = csvFieldsNumber;
    }
    async exportCSV (dir, o) {
        let pathstack = this.getDirStack(dir)
        var obj = await this.getObj(o);

        if (this.name) {
            pathstack.push(this.name);
        }

        if (this.fields.length == this.csvFieldsNumber) {
            if (this.name) {
                pathstack.pop()
            }
            let wrapper = {}
            wrapper[this.name] = obj;
            return HierarchicalData._exportCSV(
                wrapper, pathstack, ["filename",...this.fields], this.csvFieldsNumber);
        } else {
            return HierarchicalData._exportCSV(obj, pathstack, this.fields, this.csvFieldsNumber);
        }
    }
    /**
     * recursive internal method
     */
    static _exportCSV(obj, pathstack, fields, csvFieldsNumber) {
        let delta = fields.length - csvFieldsNumber;
        if (delta > 1) {
            for (let key in obj) {
                pathstack.push(key);
                this._exportCSV(obj[key],pathstack, fields.slice(1),csvFieldsNumber)
                pathstack.pop(key);
            }
        } else if (delta == 1) { // file level
            let cwd = pathstack.join("/")
            mkdir_p(cwd);
            for (let filename in obj) {
                let matrix = this._exportCSV(obj[filename],pathstack, fields.slice(1),csvFieldsNumber);
                let content = Papa.unparse(matrix);
                fs.writeFileSync(`${cwd}/${filename}.csv`, content);
            }
        }
        else if (delta == 0) { //content level
            let matrix = [
                fields
            ]
            for (let key in obj) {
                for (let line of this._exportCSV(obj[key],pathstack, fields.slice(1),csvFieldsNumber)) {
                    matrix.push ([key, ...line])
                }
            }
            return matrix
        } else {
            let acc = [];
            if (fields.length > 1) {
                for (let key in obj) {
                    for (let line of this._exportCSV(obj[key],pathstack, fields.slice(1),csvFieldsNumber)) {
                        acc.push([key, ...line])
                    }
                }
                return acc;
            } else {
                return [[obj]] // hope it's a litteral
            }
        }
    }
    parseCSV(dirOrFile) {
        let datapath = dirOrFile + "/";
        if (this.parentDirs) {
            datapath += this.parentDirs + '/';
        }
        datapath += this.name;
        if (fs.existsSync(datapath)) {
            if (fs.statSync(datapath).isDirectory()) {
                return this._parseCSVDir(datapath)
            } else {
                return this._parseCSVFile(datapath)
            }
        } else {
            return this._parseCSVFile(datapath + '.csv')
        }

    }
    _parseCSVDir(dir) {
        let obj = {}
        fs.readdirSync(dir).forEach(filename => {
            let fullpath = dir + "/" + filename
            if (filename.substr(-4) == ".csv") {
                obj[filename.substr(0,filename.length - 4)] = this._parseCSVFile(fullpath);
            } else if (fs.statSync(fullpath).isDirectory()) {
                obj[filename] = this._parseCSVDir(fullpath)
            } else {
                console.log("skip "+filename)
            }
        })
        return obj;
    }
    _parseCSVFile(filename) {
        let parsed = Papa.parse(
            fs.readFileSync(filename).toString(),
            {
                header: true,
            });
        let obj = {}
        let current;
        for (let line of parsed.data) {
            let keys = constructor.keys(line);
            current  = obj;
            let i;
            for (i = 0; i < (keys.length - 2) ; i ++) {
                let val = line[keys[i]]
                if (! current.hasOwnProperty(val)) {
                    current[val] = {}
                }
                current = current[val]
            }
            current[line[keys[i]]] = line[keys[i+1]]
        }
        return obj
    }
}

var scheme = [
    {
        desc: new HierarchicalData("grille",
            ["corps","year", "grade", "echelon", "indice"], 3, "echelons"),
    },
    {
        desc: new HierarchicalData("prime/evs",
            ["year","niveau", "montant" ], 2, "evs"),
    },
    {
        desc: new HierarchicalData("prime/tech_ieeac",
            ["year","grade", "echelon","montant" ], 3, "partTechIEEAC"),
    },
    {
        desc: new TabularData("fonction",
            ["name", "evs" ], "evsLevel"),
    },
    {
        desc: new HierarchicalData("indices/brutmajo",
            ["date","brut","majo"], 2, "table_indices"),
    },
    {
        desc: new TabularData("sites",
        ["oaci", "nom", "lon", "lat", "corps", "groupe", "liste", "region", "pcs", "transport",
         "reorgST", "xpRH", "siege", "SNA", "citycode", "mobilite" ], "sites"),
    },
]

export default {
    scheme,
    registerModule,
}
