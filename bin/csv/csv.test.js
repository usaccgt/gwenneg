
import { expect, describe, test } from 'vitest'


import {HierarchicalData} from './common';
console.log(HierarchicalData);
// var common = require("./common")
// let HierarchicalData = common.HierarchicalData

var fs = require("fs")
var os = require("os")
var path = require("path")

var Papa = require("papaparse")

var a = {
    "ieeac": {
        "2016": {
          "élève": {
            "1": "340",
            "2": "359"
          },
          "normal": {
            "1": "416",
            "2": "476"
          },
        },
        "2017": {
            "élève": {
              "1": "340",
              "2": "359"
            },
            "normal": {
              "1": "434",
              "2": "483",
            },
        },
    },
    "iessa": {
        "2017": {
            "élève": {
            "1": "321"
            },
            "stagiaire": {
            "1": "334"
            },
            "normal": {
            "1": "383",
            "2": "383"
            },
        },
        "2018": {
            "élève": {
                "1": "321"
            },
            "stagiaire": {
                "1": "334"
            },
            "normal": {
                "1": "388",
                "2": "407",
            }
        }
    }
};

// global dummy object
const o = {
    "a": "b",
    "c": "d"
};
var serializedO = "key,value\r\na,b\r\nc,d"

describe("serialize", () => {
    test ("global object", () => {
        let name = "foo";
        fs.mkdtemp(path.join(os.tmpdir(), 'foo-'), async (err, folder) => {
            let hd = new HierarchicalData(name,["key","value"],2);
            await hd.exportCSV(folder, o);
            let filename = path.join(folder,name+".csv")
            let content = fs.readFileSync(filename).toString()
            expect(content).toEqual(serializedO);
        })
    })
    test ("parameter object", () => {
        let name = "bar"
        fs.mkdtemp(path.join(os.tmpdir(), 'foo-'), async (err, folder) => {
            let hd = new HierarchicalData(name,["key","value"],2, null);
            await hd.exportCSV(folder, o);
            let filename = path.join(folder,name+".csv")
            let content = fs.readFileSync(filename).toString()
            expect(content).toEqual(serializedO);
        })
    })
})

function getEchelonDesc(depth) {
    return new HierarchicalData("grille",
        ["corps","year", "grade", "echelon", "indice"], depth, null)
}

describe("Field number", () => {
    test("two columns", async () => {
        let folder = fs.mkdtempSync(path.join(os.tmpdir(), 'foo-'))
        await getEchelonDesc(2).exportCSV(folder, a);
        let filename = path.join(folder, "grille", "ieeac", "2016", "élève.csv")
        expect(fs.existsSync(filename)).toBe(true);
        let content = fs.readFileSync(filename).toString()
        let parsed = Papa.parse(content);
        expect(parsed.data[0].length).toBe(2)
    })
    test("three columns", async () => {
        let folder = fs.mkdtempSync(path.join(os.tmpdir(), 'foo-'))
        await getEchelonDesc(3).exportCSV(folder, a);
        let filename = path.join(folder, "grille", "ieeac", "2016.csv")
        expect(fs.existsSync(filename)).toBe(true);
        let content = fs.readFileSync(filename).toString()
        let parsed = Papa.parse(content);
        expect(parsed.data[0].length).toBe(3)
    })
})

function testIsomorphismEchelons(depth) {
    let hd = getEchelonDesc(depth);
    fs.mkdtemp(path.join(os.tmpdir(), 'foo-'), async (err, folder) => {
        // Prints: /tmp/foo-itXde2
        await hd.exportCSV(folder, a)
        let b = hd.parseCSV(folder)
        expect(b).toEqual(a)
    });
}

describe("Isomorphism (export/import)", () => {
    test('two column csv', () => {
        testIsomorphismEchelons(2);
    })
    test('three columns csv', () => {
        testIsomorphismEchelons(3);
    })
    test('four columns csv', () => {
        testIsomorphismEchelons(4);
    })
});
