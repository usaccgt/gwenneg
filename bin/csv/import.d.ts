// import.d.ts

interface SchemeElement {
    desc: CsvData,
    obj: null | object;
}

interface CsvData {
    name: string,
    varname: string;
    async parseCSV(dir:string) : void,
}

export declare function importCsv(scheme ?: Array<SchemeElement>, outputdir ?: string) : void;
