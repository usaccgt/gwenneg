#! /bin/bash
#
# Build a static version and push it to gw-pages branch.
#
#

set -e

usage () {
  cat << EndOfMessage
Build a static version and push it to a git branch (by default: gw-pages)

./publish_sandcats.sh [-h] [-g] [GIT_REMOTE [GIT_TARGET_BRANCH]]

    GIT_REMOTE  the remote used to publish static site (default: origin)
     GIT_TARGET_BRANCH the branch to push static site (default: gw-pages)

    -h show this message
    -g generate js data file from CSV
    -j also update jekyll site
    -d dry run (do not push to remote)
EndOfMessage
}

while getopts "hgdj" flag; do
case "$flag" in
    h) usage ; exit -1 ;;
    g) GENERATE=1;;
    d) DRY=1;;
    j) JEKYLL=1;;
esac
done
shift $((OPTIND-1))

REMOTE_TARGET=${1-origin}
REMOTE_BRANCH=${2-"gw-pages"}


REF=$(git rev-parse --short HEAD)
REMOTE_URL=$(git remote get-url $REMOTE_TARGET)
NAME=$(git config user.name)
EMAIL=$(git config user.email)

TMPPUSH=/tmp/publish-simupaye
rm -Rf $TMPPUSH

git stash save --include-untracked
if [[ $GENERATE -eq 1 ]]; then
  (cd bin/csv;npm test)
  node bin/csv/import.js
fi
(cd tests/old; npm test)
(cd vue; node_modules/.bin/vue-cli-service test:e2e --headless)
(cd vue; npm run build)
if [[ $JEKYLL -eq 1 ]] ; then
  (cd jekyll; bundle exec jekyll build)
  (cd tests/old/frontend-form; npm test)
fi
git stash pop

git clone -c credential.helper=store $REMOTE_URL $TMPPUSH
(cd $TMPPUSH && git checkout $REMOTE_BRANCH)
cp -R vue/dist/* $TMPPUSH

if [[ $JEKYLL -eq 1 ]] ; then
  cp -R jekyll/_site/form $TMPPUSH
fi

cd $TMPPUSH
git config user.name $NAME
git config user.email $EMAIL
git add *
# exclude feed.xml from diff because lastBuildDate is always updated
if git diff --cached --quiet -- ':!form/feed.xml' ; then
  echo "nothing new."
else
  git commit -m "update site to $REF"
  if [[ $DRY -eq 1 ]] ; then
    echo "would push from:" $(pwd)
    git log -n 1 --stat
  else
    git push origin $REMOTE_BRANCH
  fi
fi
