

import fs from "fs";

const now = new Date().toISOString();

function generateSitemap(baseURL) {
    let xml = `<?xml version='1.0' encoding='UTF-8'?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${getHome(baseURL)}
${getCorps(baseURL)}
${getMDs(baseURL)}
</urlset>`
  return xml;
}

function getHome(baseURL) {
    return getUrl ({
        loc: baseURL,
        changefreq: 'weekly',
        priority: 1,
    });
}

function getUrl(u) {
    if (u.lastmod == undefined) {
        u.lastmod = now;
    }
    return `  <url>
${asXml(u, 4)}
  </url>`;
}

function asXml(o, indent=0) {
    return Object.entries(o).map( ([key, value]) => `${' '.repeat(indent)}<${key}>${value}</${key}>`).join('\n');
}

function getCorps (baseURL) {
    return ['icna', 'iessa', 'tseeac', 'ieeac'].map(c => getUrl({
        loc: `${baseURL}${c}/`,
        priority: .9,
    })).join("\n");
}
function getMDs(baseURL) {
    let mdPages = ['about', 'contributing'].map(a => {
        let stat = fs.statSync(`${a.toUpperCase()}.md`);
        let datemod = new Date(stat.mtimeMs);
        return getUrl({
            loc: `${baseURL}${a}/`,
            priority: .6,
            changefreq: 'monthly',
            lastmod: datemod.toISOString(),
        });
    });
    return mdPages.join("\n");
}

export function writeSitemap(outputFile = "sitemap.xml", baseURL = "https://simulateur.usac-cgt.org/") {
    fs.writeFileSync(outputFile, generateSitemap(baseURL));
}

// hack to detect if run or imported
if (import.meta.url.endsWith(process.argv[1])) {
    writeSitemap();
}