#! /bin/bash

set -e

DIR=". bin/mobilite bin/majFonctions tests/core"
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
REPO_ROOT="$SCRIPTPATH/.."
PREFILL_COMMIT=".git/GITGUI_MSG"

cd "$REPO_ROOT"

if [ -z "$(git status --untracked-files=no --porcelain)" ]; then
  # Working directory clean
  for D in $DIR; do
    cd "$REPO_ROOT/$D"
    pwd
    npm update && git add package-lock.json
  done
  cd "$REPO_ROOT"
  if [ ! -f "$PREFILL_COMMIT" ]; then
    echo "chore: update js dependencies" > "$PREFILL_COMMIT"
  fi
else
  echo "dépôt git contient uncommitted changes"
  exit -1
fi

git status
