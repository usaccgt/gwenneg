#! /usr/bin/node
/* tri par evs puis par nom (pour comparaison)
 * usage:
 *   sort.js fichier.csv
 * crée fichier.sorted.csv
 */


import { CSVCLIFilter } from "./filter.js"

export function sortCsvData(data) {
  return data.toSorted(
    (a, b) =>
      a.evs == b.evs ? a.name.localeCompare(b.name) : parseInt(a.evs) - parseInt(b.evs)
  );
}

// hack to detect if run or imported
if (import.meta.url.endsWith(process.argv[1])) {
  CSVCLIFilter(sortCsvData, "sorted");
}
