import {DilaApiClient} from './DilaApiClient.js';
import fs from "fs";
import os from "os";
import path from "path";
import Papa from "papaparse";

import debugModule from 'debug';
const debug = debugModule("fetchFonctions");

class LegifranceAPI extends DilaApiClient {
    constructor () {
        super();
        this.basePath = "/dila/legifrance/lf-engine-app";
    }
    async getArticle(id) {
        debug(`LegifranceAPI.getArticle(${id})`)
        return this.fetch({
            path: '/consult/getArticle',
            params: {
                "id": id,
              }
        });
    }
    async getJO(id) {
        return this.fetch({
            path: '/consult/jorf',
            params: {
                "textCid": id,
              }
        });
    }
    async getArticleEnVigueur(id) {
        debug(`LegifranceAPI.getArticleEnVigueur(${id})`)
        let result = await this.getArticle(id);
        let article = result.article;
        if (article.etat != "VIGUEUR") {
            let [ vigueur ]= article.articleVersions.filter(a => a.etat == "VIGUEUR");
            console.log(vigueur.id);
            return this.getArticle(vigueur.id);
        }
        return result;
    }
}
class LegifranceAPICached extends LegifranceAPI {
    getCachefilePath(id) {
        return path.join(os.tmpdir(), `${id}.json`);
    }
    async getArticle(id) {
        debug(`LegifranceAPICached.getArticle(${id})`);
        let filename = this.getCachefilePath(id);
        debug(filename);
        if (fs.existsSync(filename)) {
            debug("parse cached");
            return JSON.parse(fs.readFileSync(filename, 'utf8'));
        } else {
            debug("fetch")
            let content = await super.getArticle(id);
            debug("save cache");
            fs.writeFile(filename, JSON.stringify(content), err => {
                if (err) {
                  console.error(err);
                } else {
                  // file written successfully
                    }
            });
            return content;
        }
    }
}

class ArticleListeFonctionProxy {
    constructor(api, id) {
        this.api = api;
        if (id == undefined) {
            this.id = "LEGIARTI000049948678";
            this.fetchArticleEnVigueur = true;
        } else {
            this.id = id;
        }
    }
    async retrieve () {
        if (!this.result) {
            if (this.fetchArticleEnVigueur) {
                 this.result = await this.api.getArticleEnVigueur(this.id);
            } else {
                this.result = await this.api.getArticle(this.id);
            }
        }
        return this.result.article;
    }
    cleanFonction(line) {
        let stripArticle = /^(Les |Le |L')(.*?)( ;|\.|)$/m
        let m = line.match(stripArticle);
        if (m) {
            line = m[2];
        } else {
            console.log(`pas match:\n'${line}'`);
        }
        let stripHtml = /(<([^>]+)>)/ig;
        line = line.replace(stripHtml,"");
        return line[0].toUpperCase() + line.slice(1);
    }

    async getFonctionsListe() {
        if (!this.fonctionsByNiveau) {
            const content = (await this.retrieve()).texteHtml;
            const sep1 = '<br/>\n';
            const sep2 = '<br/>';
            let separator = content.indexOf(sep1) != -1 ? sep1 : sep2;
            let tokens = content.split(separator);
            tokens = tokens.slice(1);
            this.fonctionsListe = [];
            let niveau = undefined;
            const regex = /^[IVX]+.*niveau ([0-9]+) :$/m;
            tokens.forEach(line => {
                line = line.trim();
                let m = line.match(regex);
                if (m) {
                    niveau=m[1];
                } else if (niveau != undefined) {
                    this.fonctionsListe.push({name:this.cleanFonction(line),evs:niveau});
                }
            });
        }
        return this.fonctionsListe;
    }
}

async function downloadFonctionsHistory() {
    let apiEndPoint = new LegifranceAPICached();
    let article = new ArticleListeFonctionProxy(apiEndPoint);

    let content = await article.retrieve();

    for(const [k,v] of Object.entries(content.articleVersions)) {
        if (v.version != "INITIALE") {

            let anArticle = new ArticleListeFonctionProxy(apiEndPoint, v.id);
            let filename = `${new Date(v.dateDebut).toISOString().slice(0, 10)}.csv`
            let csvContent = Papa.unparse(await anArticle.getFonctionsListe(), {columns: ["name", "evs"] })
            fs.writeFile(filename, csvContent, (err) => {
                if (err) throw err;
            });
        }
    }
}

// hack to detect if run or imported
if (import.meta.url.endsWith(process.argv[1])) {
    downloadFonctionsHistory();
}