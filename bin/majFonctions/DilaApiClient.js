
/* adapted from https://gitlab.com/piste_lab/oauth_connectors */

import fetch from 'node-fetch';
import { ClientCredentials } from 'simple-oauth2';
import debugModule from 'debug';
const debug = debugModule("dila-api-client");

const clientId = process.env.GWENNEG_OAUTH_CLIENT_ID;
const clientSecret = process.env.GWENNEG_OAUTH_CLIENT_SECRET;

const apiHost =
  "https://sandbox-api.piste.gouv.fr/dila/legifrance/lf-engine-app";
const tokenHost = "https://sandbox-oauth.piste.gouv.fr";

const credentials = {
  client: {
    id: clientId,
    secret: clientSecret
  },
  auth: {
    tokenHost,
    tokenPath: "/api/oauth/token",
  },
  options: {
    authorizationMethod: "body"
  }
};

export class DilaApiClient {
  constructor() {
    // for tests
    this.apiHost = apiHost;
    this.tokenHost = tokenHost;
  }

  async getAccessToken() {
    debug(`getAccessToken | this.globalToken: ${this.globalToken}`)
    if (this.globalToken) {
      return this.globalToken;
    }
    const client = new ClientCredentials(credentials);
    try {
      const accessToken = await client.getToken({
        scope: "openid"
      });
      this.globalToken = accessToken.token.access_token;
      return accessToken.token.access_token;
    } catch (error) {
      debug("error", error);
      debug("Access Token error", error.message);
    }
  }

  async fetch({ path, method = "POST", params }) {
    const [routeName] = path.split("/").slice(-1);
    const body = JSON.stringify(params);
    debug(`fetching route ${routeName} with ${body}...`);
    const token = await this.getAccessToken();
    const url = `${apiHost}/${path}`;
    const data = await fetch(url, {
      method,
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body
    })
      .then(r => r.json())
      .then(data => {
        if (data.error) {
          throw `Error on API fetch: ${JSON.stringify(data)}`;
        } else return data;
      })
      .catch(e => {
        debug("ERROR", e);
        debug({
          url,
          body,
          token
        });

        throw e;
      });

    return data;
  }
}
