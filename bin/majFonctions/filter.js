/* util function to manipulate csv files */

import fs from "fs";
import Papa from "papaparse";

export function transformCSV(inputFile, outputFile, editFn) {
  console.log(`sortFonctionsCSV(${inputFile}, ${outputFile})`);
  let parsed = Papa.parse(
    fs.readFileSync(inputFile).toString(),
    {
      header: true,
    });
  let content = editFn(parsed.data);
  let csvContent = Papa.unparse(content, { columns: ["name", "evs"] })
  fs.writeFile(outputFile, csvContent, (err) => {
    if (err) throw err;
  });
}

export function CSVCLIFilter(editFn, suffix = "output") {
  let filename = process.argv[2];
  if (!fs.existsSync(filename)) {
    throw new Error(`Fichier ${filename} n'existe pas`);
  }
  if (!filename.match(/\.csv$/)) {
    throw new Error("mauvaise extension (.csv attendu)");
  }
  let outputFile = filename.replace(".csv", `.${suffix}.csv`)
  transformCSV(filename, outputFile, editFn);
}