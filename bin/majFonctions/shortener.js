#! /usr/bin/node
/* applique simplification décrite dan assoBrutCourt.csv
 * usage:
 *   shortener.js fichier.csv
 * crée fichier.short.csv
 */

import fs from "fs";
import Papa from "papaparse";

import { CSVCLIFilter } from "./filter.js"
import { createSourceMapSource } from "typescript";

function createAssoc(filename = "assoBrutCourt.csv") {
  let parsed = Papa.parse(
    fs.readFileSync(filename).toString(),
    {
      header: true,
    });
  let acc = {};
  parsed.data.forEach(line =>
    acc[line.brut] = line.court
  )
  return acc;
}

function shorten(data) {
  let asso = createAssoc();
  return data.map(v => ({name: asso[v.name]??v.name, evs: v.evs}));
}


// hack to detect if run or imported
if (import.meta.url.endsWith(process.argv[1])) {
  CSVCLIFilter(shorten, "short");
}