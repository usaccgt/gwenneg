// import.d.ts

interface YamlData {
    varname: string;
    filename: string;
  }

export declare function importYaml(scheme ?: Array<YamlData>, outputdir ?: string) : void;
