import { reactive } from 'vue';
import { createStore } from 'vuex';

import router from '@/router/index';

import debugModule from 'debug';
const debug = debugModule("gwenneg:front:store");

import deducers from '@/store/deducers';
import { PaySheetComputer } from '../../../src/old_js/core';
import { DataProviderFactory } from '../../../src/old_js/data';

import { getSiteFromId } from '../../../src/old_js/sites';
import {
  Observer,
  isValidFrenchdateStr,
  dateToFrenchDatestr,
} from '../../../src/old_js/utils';
/* eslint-disable no-use-before-define */
/* eslint-disable no-param-reassign */

const handlerMixin = {
  get(obj, prop) {
    if (prop in obj) {
      return obj[prop];
    }
    if (prop in state.deduced) {
      return state.deduced[prop];
    }
    if (prop in state.default) {
      return state.default[prop];
    }
    return undefined;
  },
  set(obj, prop, val) {
    debug(`set(${prop},${val})`);
    const idx = state.actions.findIndex((a) => a[0] === 'set' && a[1] === prop);
    if (idx !== -1) {
      state.actions.splice(idx, 1);
    }
    state.actions.push(['set', prop, val]);
    /* callback is required to handle batch property change
     ie, if two or more properties are pushed to route
     in a raw, only the last one is visible in url */
    setTimeout(() => updateRoute(prop, val), 0);
    obj[prop] = val;
    this.fire(prop, state);
    return true;
  },
  ownKeys() {
    return [...new Set([
      ...state.actions.map((a) => a[1]),
      ...Reflect.ownKeys(state.deduced),
      ...Reflect.ownKeys(state.default),
    ])];
  },
  getOwnPropertyDescriptor(target, key) {
    return { value: this.get(target, key), enumerable: true, configurable: true };
  },
};

const handler = new Observer();
Object.assign(handler, handlerMixin);

deducers.forEach((d) => { d.subscribeOn(handler); });

async function updateRoute(prop, val) {
  const options = {
    path: router.currentRoute.value.path,
    query: { ...router.currentRoute.value.query},
  };
  if (prop == 'corps') {
    options.path = `/${val}/`;
    options.force = true;
  } else {
    options.query[prop] = val;
  }
  await router.push(options);
}
async function removeFromRoute(prop) {
  const newQuery = { ...router.currentRoute.value.query };
  delete newQuery[prop];
  await router.push({
    name: router.currentRoute.value.name,
    query: newQuery,
  });
}

const state = {
  default: {
    corps: 'iessa',
    validity: dateToFrenchDatestr(new Date()),
    grade: 'élève',
    echelon: 1,
    famille: 0,
    licence: 0,
    encadrement: 0,
    AeIessa: 0,
    sortieENAC: 0,
    QTPourMajoGeographique: 0,
    attractivite2546: 0
  },
  actions: [],
  deduced: reactive({}),
  input: new Proxy({
    get siteData() {
      return getSiteFromId(this.site);
    },
    getStatus(prop) {
      if (prop in this) {
        return 'input';
      }
      if (prop in state.deduced) {
        return 'deduced';
      }
      if (prop in state.default) {
        return 'default';
      }
      return 'notset';
    },
  }, handler),
  dp: DataProviderFactory.getInstance(),
};

const c = new PaySheetComputer(state.dp);

function compute(input) {
  state.dp.setDate(input.validity);
  return c.compute_income(input);
}

const specialActions = {
  site: 'setSite',
  validity: 'setDate',
};

function handleQuery(context, queryObj) {
  setTimeout(() => {
    delayedHandleQuery(context, queryObj);
  }, 0);
}

async function delayedHandleQuery(context, queryObj) {
  Object.entries(queryObj).forEach(
    async ([key, value]) => {
      if (key in specialActions) {
        await context.dispatch(specialActions[key], value);
      } else {
        context.state.input[key] = value;
      }
    },
  );
}

async function resetState(context) {
  if (context && false) {
    /* FIXME this approach avoir reload but need
      to reset all inputs => including custom widget */
    context.state.actions = [];
    context.state.deduced = {};
    context.state.input = new Proxy({
      get siteData() {
        return getSiteFromId(this.site);
      },
    }, handler);
  }
  await router.push({
    name: router.currentRoute.value.name,
    query: {},
  });
  window.scrollTo(0, 0);
  window.location.reload();
}

export default createStore({
  state,
  getters: {
    paysheet(s) {
      return compute(s.input);
    },
  },
  /* eslint-disable no-param-reassign */
  mutations: {
    setValidity(s, aDate) {
      s.input.validity = aDate;
    },
    setSite(s, newSiteCode) {
      const newSite = getSiteFromId(newSiteCode);
      s.input.site = newSiteCode;
      s.deduced.region = newSite.region;
      s.deduced.groupe = newSite.groupe;
      s.deduced.liste = newSite.liste;
      if (newSite.reorgST) {
        s.deduced.reorg = newSite.reorgST;
      } else {
        delete s.deduced.reorg;
      }
      if (newSite.xpRH) {
        s.deduced.xpRHFromSite = newSite.xpRH;
      } else {
        delete s.deduced.xpRHFromSite;
      }
    },
    deleteInput(s, prop) {
      delete s.input[prop];
      setTimeout(() => removeFromRoute(prop), 0);
    },
  },
  actions: {
    setDate(context, aDate) {
      if (isValidFrenchdateStr(aDate)) {
        return context.commit('setValidity', aDate);
      }
      return true;
      // FIXME choose to accept any date or only significant
      // if (dp.validityDates.includes(aDate)) {
      // return context.commit('setValidity', aDate);
      // }
      // return Promise.reject(Error(`date ${aDate} invalide\nvalides: ${dp.validityDates}`));
    },
    setSite(context, aSite) {
      return context.commit('setSite', aSite);
    },
    handleQuery,
    resetState,
  },
  modules: {
  },
});
