import { Fonctions } from '../../../src/old_js/evs';

class Deducer {
  constructor(hooks, d) {
    this.hooks = hooks;
    this.deduce = d;
  }

  subscribeOn(o) {
    this.hooks.forEach((element) => {
      o.bind(element, this.deduce.bind(this));
    });
  }
}

function licenceAvantProtocole2023 (s) {
  let niveauLic = 0;
  if (s.input.corps === "tseeac") {
    if (s.input.groupe) {
      niveauLic = {
        "G": 101,
        "F": 102,
      }[s.input.groupe[0]];
    }
    return niveauLic;
  }
  // icna
  switch (s.input.grade) {
    case 'élève':
      break;
    case 'stagiaire':
    case 'normal':
      if (['LFPG', 'LFPO'].includes(s.input.site)
          && s.input.qualifLOC
          && s.input.qualifLOC !== '0') {
        niveauLic = s.input.grade === 'stagiaire' ? 1 : 4;
      }
      break;
    default:
      if (s.input.groupe) {
        niveauLic = {
          G: 2,
          F: 3,
          E: 4,
          D: 4,
          C: 4,
          B: 5,
          A: 5,
        }[s.input.groupe[0]];
      }
  }
  return niveauLic;
}

function licenceApresProtocole2023(s) {
  let niveauLic = 0;
  if (s.input.corps === 'icna') {
    switch (s.input.grade) {
      case 'élève':
        break;
      case 'stagiaire':
      case 'normal':
        if (['LFPG', 'LFPO'].includes(s.input.site)
            && s.input.qualifLOC
            && s.input.qualifLOC !== '0') {
          niveauLic = s.input.grade === 'stagiaire' ? 1 : 4;
        }
        break;
      default:
        if (s.input.liste) {
          niveauLic = {
            11: 2,
            10: 3,
            9: 6,
            8: 4,
            7: 4,
            6: 6,
            5: 6,
            4: 6,
            3: 5,
            2: 5,
            1: 5,
          }[s.input.liste];
        }
    }
  }
    else if (s.input.corps === 'tseeac') {
      if (s.input.liste && [9, 10, 11].includes(Number(s.input.liste))) {
        niveauLic = {
          11: 101,
          10: 102,
          9: 103,
        }[s.input.liste];
      }
    }
  return niveauLic;
};


/* eslint-disable no-param-reassign */
const licenceDeducer = new Deducer(
  ['corps', 'site', 'grade', 'qualifLOC'],
  (s) => {
    if(['icna', 'tseeac'].includes(s.input.corps)) {
      if (s.dp.isBeforeProtocole2023()) {
        s.deduced.licence = licenceAvantProtocole2023(s);
      } else {
        s.deduced.licence = licenceApresProtocole2023(s);
      }
    }
    else {
      delete s.deduced.licence;
    }
  },
);

const pcsDeducer = new Deducer(
  ['site', 'corps'],
  (s) => {
    if (s.input.siteData?.pcs === '1') {
      // par défaut: opérationnel
      s.deduced.pcs = 150;
      if (s.input.corps === 'ieeac') {
        s.deduced.pcs = 100;
      }
      if (s.input.corps === 'tseeac') {
        if (s.input.siteData.estTerrainTS()) {
          s.deduced.pcs = 150;
        } else {
          s.deduced.pcs = 100;
        }
      }
    } else {
      delete s.deduced.pcs;
    }
  },
);

const nbiDeducer = new Deducer(
  ['corps', 'grade', 'echelon'],
  (s) => {
    if (['en chef', 'détaché'].includes(s.input.grade)
    || (
      s.input.corps === 'iessa'
      && s.input.grade === 'divisionnaire'
      && (parseInt(s.input.echelon, 10) > 6))
    ) {
      s.deduced.nbi = 1;
    } else {
      delete s.deduced.nbi;
    }
  },
);


const peqDeducer = new Deducer(
  ['corps', 'grade', 'echelon'],
  (s) => {
    if (s.input.corps === 'iessa') {
      switch (s.input.grade) {
        case 'élève':
        case 'stagiaire':
          delete s.deduced.peq;
          break;
        case 'normal':
          s.deduced.peq = 1;
          break;
        case 'divisionnaire':
          s.deduced.peq = parseInt(s.input.echelon, 10) >= 7 ? 5 : 3;
          break;
        default:
          s.deduced.peq = 5;
      }
    } else {
      delete s.deduced.peq;
    }
  },
);

const QTPourMajoGeographiqueDeducer = new Deducer(
  ['site', 'corps', 'grade', 'echelon'],
  (s) => {
    if (s.input.corps === 'iessa' && ['LFEE', 'LFFF', 'LFPO', 'LFSB', 'LFOB', 'STAB', 'LFPB', 'BEA', 'LFQQ', 'LFPM', 'LFPG', 'LFST', 'LFSD', 'LFPN'].includes(s.input.site)) {
      switch (s.input.grade) {
        case 'élève':
        case 'stagiaire':
          delete s.deduced.QTPourMajoGeographique;
          break;
        case 'normal':
          s.deduced.QTPourMajoGeographique = s.input.echelon > 2 ? 2 : 0;
          break;
        default:
          // QT + 4 ans
          s.deduced.QTPourMajoGeographique = 2;
      }
    } else {
      delete s.deduced.QTPourMajoGeographique;
    }
  },
);

const fonctionDeducedFields = ["AeIessa"]
const evsDeducer = new Deducer(
  ['fonction', 'site'],
  (s) => {
    let f = Fonctions[s.input.fonction];
    if (f) {
      if (f.niveau == undefined) {
        f = f(s.input.siteData)
      }
      s.deduced.evs = f.niveau;
      fonctionDeducedFields.forEach(field => {
        if (field in f) {
          s.deduced[field] = f[field];
        } else {
          delete s.deduced[field]
        }
      })
      s.deduced.encadrement = (f.encadrement || s.input.showEncadrement);
    } else {
      delete s.deduced.evs;
    }
  },
);

const primeEncadrementDO = new Deducer(
  ['fonction', 'site'],
  (s) => {
    if (s.input && s.dp.isAfterProtocole2023()) {
      let f = Fonctions[s.input.fonction];
      if (f && f.PrimeEncDO) {
        s.deduced.PrimeEncDO = f.PrimeEncDO;
        if(s.input.fonction === "ChefService" && s.input.siteData.liste <= 3) {
          s.deduced.PrimeEncDO = 1;
        }
      } else {
        delete s.deduced.PrimeEncDO;
      }
    } else {
      delete s.deduced.PrimeEncDO;
    }
  }
)

const sortiePromoNordNordEst = new Deducer(
  ['site'],
  (s) => {
    if (s.input.siteData?.estDansZoneAttractivite2023()) {
      s.deduced.sortieENAC = 1;
    } else {
      delete s.deduced.sortieENAC;
    }
  }
)

/* based on https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000044593152
fonctions presence is checked in evs.test.js to avoid regression
*/
const ctacableDeducer = new Deducer(
  ['fonction', 'site'],
  (s) => {
    const site = s.input.site;
    const fonction = s.input.fonction
    if (['ChefPole','ChefSub', 'ExpertSenior','AssSub'].includes(fonction)
      || site == 'DTI' && fonction == 'ExpertConf'
      || site == 'ENAC' && ['IE', 'EnsConf', 'EnsSenior'].include(fonction)) {
      s.deduced.CTACable = 1;
    } else {
      delete s.deduced.CTACable;
    }
  }
)

const deducers = [
  licenceDeducer,
  pcsDeducer,
  nbiDeducer,
  peqDeducer,
  QTPourMajoGeographiqueDeducer,
  evsDeducer,
  primeEncadrementDO,
  sortiePromoNordNordEst,
  ctacableDeducer,
];

export default deducers;
