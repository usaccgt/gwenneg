import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    alias: ['/icna', '/ieeac', '/iessa', '/tseeac'],
  },
  {
    path: '/about',
    name: 'about',
    meta: {
      title: "À Propos",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/AboutView.vue'),
  },
  {
    path: '/contributing',
    name: 'contributing',
    meta: {
      title: "En cas de problème",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/ContributingView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

const siteTitle = 'Simulateur de paye USACcgt';
const siteDescription = `L'USACcgt met a disposition de tous les agents des corps techniques de la DGAC (ICNA/IEEAC/IESSA/TSEEAC) ce
simulateur de paye. Pour que chaque agent puisse comprendre sa fiche de paye, chaque ligne de ce simulateur renvoi vers un wiki explicatif.
Cet outil permet de détecter des erreurs, des oublis... et permet de se projeter sur une évolution de carrière en anticipant
d’éventuels changements de rémunération`;


function setTitle(title) {
  document.title = title;
  document
        .querySelector('meta[property="og:title"]')
        ?.setAttribute('content', title);
}

function setDescription(desc) {
  document
        .querySelector('meta[name="description"]')
        ?.setAttribute('content', desc);
}

router.beforeEach((to, from) => {
  if (to.name == 'home' && to.path != '/') {
    /* alias pour un corps */
    let match = to.path.match(/\/(ieeac|icna|tseeac|iessa)\//)
    if (match) {
      let corps = match[1];
      setTitle(corps.toUpperCase());
      setDescription(`Simulateur de salaire pour les ${corps.toUpperCase()}.`);
    }
    return;
  }
  if (to.meta.title) {
    setTitle(`${to.meta.title} | ${siteTitle}`)
  } else {
    setTitle(siteTitle);
  }
  if (to.meta.desc) {
    setDescription(to.meta.desc)
  } else {
    setDescription(siteDescription);
  }
})

export default router;
