/**
 * Copyright (C) 2016-2022 Bruno Spyckerelle
 * Copyright (C) 2022-2023 USACcgt
 * License AGPL 3.0
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <https://www.gnu.org/licenses/>
 *
 */

import {
    Observer,
    compareFrenchDateStr,
    yamlDatestrToDate,
    dateToFrenchDatestr,
} from "./utils.js";

import moment from 'moment';

import {echelons, table_indices, evs, partTechIEEAC, evsLevel} from "./datas/generated.js"

import {
    indice_100,
    rpc_rate,
    rpc_pat_rate,
    indemnite_nuit,
    indemnite_teletravail,
    indemnite_teletravail_max,
    indemnite_forfaire_astreinte_2003,
    indice_reference_ifa,
    pcs,
    points_nbi,
    validityDates,
    transfertRetenue,
    csg_deduc,
    modulationRMA,
    modulationCDGIessa,
    modulationCDGTseeac,
    primeEncadrementDO,
    modulationCDGIcna,
    modulationCDGIcnaLevel,
    modulationLFRRIcna,
    domtom,
    sft_fixe,
    sft_prop,
    sft_bound,
    tauxAboTransport,
} from './datas/const.js';

import {
    modulationGeoIEEAC,
    partExperience,
    partTechIESSA,
    partTechIESSALevel,
    partTechTSQualif,
    partTechTSQualifLevel,
    partTechTSHabil,
    partTechTSHabilLevel,
    expe2016,
    expe2016IessaLevel,
    partLicence,
    partLicenceLevel,
    complementPartLicence,
    majoComplementPartLicence,
    xpRH2016,
    attractiviteSortieEnac,
    fidelisationAthisReimsOrly,
} from './datas/grilles.js';

import {
    grades,
    detachements,
    detachementsByCorps,
} from './datas/grades.js';

import * as yaml from '../datas/generated.js';

var csgDate = moment('2018-01-01');
var irDate = moment('2019-01-01');

/**
 * Centralize data access for paysheet computing.
 *
 * Handle time-dependent data (and fill gaps in definition,
 * ie provide last valid version when no data is present
 * for a given year).
 *
 * Fire "data-update" when date is changed
 *
 */
class DataProvider extends Observer {
    constructor(aDate) {
        super();
        this.setDate(aDate);
        // FIXME access through globals
        this.yearlyData = {
            evs,
            partTechIEEAC,
            partTechTSQualif,
            modulationGeoIEEAC,
            partExperience,
            rpc_rate,
            rpc_pat_rate,
            csg_deduc,
            primeEncadrementDO,
            modulationCDGIessa,
            modulationCDGTseeac,
            xpRH2016,
            modulationLFRRIcna,
            sft_bound,
        };
        this.validityDates = validityDates;
        this.grades = grades;
        this.detachements = detachements;
        this.detachementsByCorps = detachementsByCorps;
        this.echelons = echelons;
        this.domtom = domtom;
        this.points_nbi = points_nbi;
        this.transfertRetenue = transfertRetenue;
        this.evsLevel = evsLevel;
        this.sft_fixe = sft_fixe;
        this.sft_prop = sft_prop;
        this.indemnite_nuit = indemnite_nuit;
        this.partTechIESSALevel = partTechIESSALevel;
        this.indemnite_forfaire_astreinte_2003 = indemnite_forfaire_astreinte_2003;
        this.indice_reference_ifa =  indice_reference_ifa;

        this.partYearData = {
            indice_100,
            point_indice: adaptIndice(indice_100),
            table_indices: {data: table_indices, format: 'YYYY-MM-DD'},
            pcs,
            tauxAboTransport,
            indemnite_teletravail,
            indemnite_teletravail_max,
            partTechIESSA,
            fidelisationAthisReimsOrly,
            attractiviteSortieEnac,
            partTechTSHabil,
            partTechTSHabilLevel,
            partLicence,
            partLicenceLevel,
            complementPartLicence,
            majoComplementPartLicence,
            expe2016,
            expe2016IessaLevel,
        }

        this.setupGetters();
    }

    setupGetters() {
        for (let key in this.yearlyData) {
            Object.defineProperty(this, key, {
                get: () => {
                    return this.get_yearly_data(this.yearlyData[key], this.year);
                }
            });
        }
        for (let key in this.partYearData) {
            Object.defineProperty(this, key, {
                get: () => {
                    return this.get_part_year_data(this.partYearData[key], this.currentDate);
                }
            });
        }
        for (let key in yaml) {
            Object.defineProperty(this, key, this.get_yaml_getter(yaml[key]));
        }
    }

    setDate(aDate) {
        if (aDate != this.currentDate) {
            this.currentDate = aDate;
            this.currentMoment = moment(aDate, 'DD/MM/YYYY');
            this.fire("data-update", this);
        }
    }
    suscribeValidityChange(aValidityObservable) {
        aValidityObservable.bind("validity", this.updateValidity, this);
    }
    updateValidity(newValue) {
        console.log("DataProvider | updateValidity | "+newValue);
        this.setDate(newValue)
    }

    get year() {
        return this.currentMoment.year();
    }

    getEchelons(corps,grade,ef) {
        if (grade != null) {
            if (grade.localeCompare("détaché")==0) {
                if (ef != null) {
                    return this.get_yearly_data(this.echelons["detachement"])[ef];
                }
            } else {
                return this.get_yearly_data(this.echelons[corps])[grade]
            }
        }
    }
    getGrille(corps) {
        return this.get_yearly_data(this.echelons[corps])
    }
    getGrades(corps) {
        return Object.keys(this.getGrille(corps));
    }

    get_yearly_data(anObj, aYear) {
        if (aYear == undefined) {
            aYear = this.year;
        }
        if (aYear in anObj) {
            return anObj[aYear];
        }
        if (typeof aYear == "string") {
            aYear = parseInt(aYear)
        }
        for (var z = aYear; z--; z >= 2015) {
            if (z in anObj) {
               anObj[aYear] = anObj[z]
                return anObj[aYear];
            }
        }
        throw "error!";
    }

    get_yaml_getter(anObj) {
        // TODO search in sparse structure instead of creating redundant dense
        // dataset
        let converted = this.convert_yam_to_part_year(anObj);
        return {
            get: () => {
                return this.get_part_year_data(converted, this.currentDate);
            }
        };
    };

    /**
     * Transform yaml sparse data in part_year compliant object.
     * For example
     *  20230101:
     *    a: 1
     *    b: 2
     *  20240101:
     *    b: 4
     *    c: 5
     * is converted to:
     *  {
     *    "01/01/2023" :{
     *        a:1,
     *        b:2
     *    },
     *    "01/01/2024": {
     *        a: 1,
     *        b: 4,
     *        c: 5
     *    }
     * }
     *
     * @param {*} anObj with YYYYMMDD keys
     * @returns part_year compliant object (DD/MM/YYYY keys and dense data)
     */
    convert_yam_to_part_year(anObj) {
        let keys = Object.keys(anObj);
        let dates = keys.map(d => {
            return {k:d,d:yamlDatestrToDate(d)}
        })
        let acc = {};
        let last_acc = {};
        let regular_part_year = {};
        dates.forEach((d, index) => {
            Object.assign(acc, anObj[d.k]);
            if (index > 0) {
                Object.keys(anObj[d.k]).forEach(parentProp => {
                    //if parentItem exist before, parse  it to ensure keeping "old" properties that are not defined more recently
                    //TODO ? Make it recursive ?
                    if(last_acc[parentProp]) {
                        Object.keys(last_acc[parentProp]).forEach(oldProp => {
                            if(!(oldProp in anObj[d.k][parentProp])) {
                                acc[parentProp][oldProp] = last_acc[parentProp][oldProp];
                            }
                        });
                    }
                });
            }

            Object.assign(last_acc, acc);
            regular_part_year[dateToFrenchDatestr(d.d)] = Object.assign({},acc);
        });
        return regular_part_year;
    }

    get_part_year_data(anObj, aDate) {
        const dateFormat = "D/M/YYYY";
        let dateFormatParse;
        if ("format" in anObj) {
            dateFormatParse = anObj.format;
            anObj = anObj.data;
        } else {
            dateFormatParse = dateFormat;
        }
        const originalDate = aDate
        if (aDate == undefined) {
            aDate = this.currentMoment;
        } else if (aDate in anObj) {
            return anObj[aDate]
        } else if (typeof aDate == "string") {
            aDate = moment(aDate, dateFormat)
        }
        const key = aDate.format(dateFormatParse)
        if (key in anObj) {
            return anObj[key];
        } else {
            let keys = Object.keys(anObj);
            let moments = keys.map(d => {
                return {k:d,m:moment(d, dateFormatParse)}
            })
            let invSortedMoments = moments.sort((a,b)=>b.m-a.m)
            for (let i = 0; i < invSortedMoments.length; i++) {
                if(aDate >= invSortedMoments[i].m) {
                    anObj[key] = anObj[invSortedMoments[i].k];
                    return anObj[key];
                }
            }
        }
    }
    beforeCSG() {
        return this.currentMoment < csgDate;
    }
    beforeIR() {
        return this.beforeIR < irDate;
    }
    isBeforeProtocole2023() {
        return this.isBefore("01/07/2024");
    }
    isAfterProtocole2023() {
        return this.isAfter("01/07/2024");
    }
    isBefore(frenchDateStr) {
        return compareFrenchDateStr(this.currentDate, frenchDateStr) > 0;
    }
    isAfter(frenchDateStr) {
        return compareFrenchDateStr(this.currentDate, frenchDateStr) <= 0;
    }
}

export class DataProviderFactory {
    static #instance = null;
    static getInstance(date) {
        if (this.#instance == null) {
            date ??= dateToFrenchDatestr(new Date());
            this.#instance = new DataProvider(date);
        } else if (date) {
            this.#instance.setDate(date);
        }
        return this.#instance;
    }
}

function adaptIndice(ind100) {
    let copy = {...ind100};
    Object.keys(copy).forEach(key =>
        copy[key] = Math.round(copy[key] / 1200 * 1e5)/1e5
    )
    return copy;
}
