
import { sites } from "./datas/generated.js"

export function getSitesFromCorps(name) {
    let acc = [];
    Object.keys(parsedSites).forEach(key => {
        let s = parsedSites[key];
        if (s.corps == "tous"
            || (Array.isArray(s.corps) && s.corps.includes(name))
            || s.corps === name) {
                acc.push(s);
        }
    });
    return acc;
};

export function getSiteFromId(id) {
    return parsedSites[id];
}

export function getAllSites() {
  return Object.values(parsedSites);
}

const grandCentre = ["LFFF", "LFEE", "LFMM", "LFBB", "LFRR", "LFPG", "LFPO", "CESNAC"]
//https://lannuaire.service-public.fr/gouvernement/8c94b894-935c-472c-9ba8-be4f4aa7dda0
const siegeDSAC = ["FARM", "TFFF", "LFLL", "LFFF", "LFOB", "FMEE", "LFBB", "LFBO", "LFRB", "LFMM", "LFPG"]

const zoneAttractivite2023 = ['LFEE', 'LFFF', 'LFPO', 'LFSB', 'LFOB', 'STAB', 'LFPB', 'BEA', 'LFQQ', 'LFPM', 'LFPG', 'LFST', 'LFSD', 'LFPN']

class Site {
  constructor(obj) {
    Object.assign(this, obj);
  }
  static fromJSON(jsonSite) {
    let clone = {... jsonSite}
    if (jsonSite.corps.indexOf(';') != -1) {
        clone.corps = jsonSite.corps.split(';');
    }
    if (jsonSite.transport) {
        clone.transport = {}
        jsonSite.transport.split(';').forEach(t => {
            let [abo,cout] = t.split(':')
            clone.transport[abo] = parseFloat(cout);
        })
    }
    return new Site(clone);
  }

  estDACNC() {
    return ["NWWW", "NWWM"].includes(this.oaci);
  }
  estSEAC() {
    return ["NTTB","NTMM", "NTTR", "NTAA"].includes(this.oaci);
  }
  estSiegeSNA() {
    return this.siege == 1 || grandCentre.includes(this.oaci);
  }
  estSiegeDSAC() {
    return siegeDSAC.includes(this.oaci);
  }
  estGroupeA() {
    return this.groupe[0] == 'A';
  }
  estCRNA() {
    return ['LFRR', 'LFFF', 'LFEE', 'LFBB', 'LFMM'].includes(this.oaci);
  }
  estTerrainICNA() {
    return this.groupe[0] < "F";
  }
  estTerrainTS() {
    return ['F', 'G'].includes(this.groupe[0]);
  }
  estDansZoneAttractivite2023() {
    return zoneAttractivite2023.includes(this.oaci);
  }
}

let parsedSites = {}

sites.forEach(s => {
    if (! s.nom) {
        return;
    }
    parsedSites[s.oaci] = Site.fromJSON(s);
})