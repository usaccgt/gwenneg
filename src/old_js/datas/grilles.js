

export const modulationGeoIEEAC = {
    "2017" : {
        "10": 61.14,
        "11": 68.25,
        "12": 80.04,
        "13": 85.28,
        "14": 89.46,
        "15": 93.31
    }
};

export const partExperience = {
    "2017": {
        //Personnels stagiaires
        "1": 258.44,
        //TSEEAC N
        "2": 309.84,
        //TSEEAC P ou N avec 1e qualification + 1 an, IESSA N, ICNA N
        "3": 361.46,
        //TSEEAC E ou P avec 2e qualification + 1 an, IESSA P, ICNA P et IEEAC N
        "4": 432.85,
        //IESSA D ou C, ICNA D ou C, IEEAC P ou HC, RTAC ou CTAC
        "5": 532.77
    },
    "2023": {
        //Personnels stagiaires
        "1": 267.49,
        //TSEEAC N
        "2": 320.68,
        //TSEEAC P ou N avec 1e qualification + 1 an, IESSA N, ICNA N
        "3": 374.11,
        //TSEEAC E ou P avec 2e qualification + 1 an, IESSA P, ICNA P et IEEAC N
        "4": 448.00,
        //IESSA D ou C, ICNA D ou C, IEEAC P ou HC, RTAC ou CTAC
        "5": 551.42,
    },
    "2024": {
        //Personnels stagiaires
        "1": 271.50,
        //TSEEAC N
        "2": 325.49,
        //TSEEAC P ou N avec 1e qualification + 1 an, IESSA N, ICNA N
        "3": 379.72,
        //TSEEAC E ou P avec 2e qualification + 1 an, IESSA P, ICNA P et IEEAC N
        "4": 454.72,
        //IESSA D ou C, ICNA D ou C, IEEAC P ou HC, RTAC ou CTAC
        "5": 559.69,
    },
};

//niveau Montant Conditions
//1 170,00 Titulaires de la QT
//2 689,99 Titulaires de la QTS et service non réorganisé
//3 744,18 Titulaires de la QTS et service réorganisé
//4 1 033,19 Titulaires de la QTS + 10 ans et service non réorganisé
//5 1 126,40 Titulaires de la QTS + 10 ans et service réorganisé

export const partTechIESSA = {
    "1/1/2019": {
        1: 170.00,
        2: 689.99,
        3: 744.18,
        4: 1033.19,
        5: 1126.40
    },
    "1/1/2023": {
        1: 175.95,
        2: 714.14,
        3: 770.23,
        4: 1069.35,
        5: 1165.82,
    },
    "1/1/2024": {
       1: 194.10,
       2: 787.81,
       3: 849.69,
       4: 1179.66,
       5: 1286.09,
    },
    "1/7/2024": {
        1: 294.10,
        2: 887.81,
        3: 949.69,
        4: 1279.66,
        5: 1386.09,
    },
}

export const partTechIESSALevel = [
    {name: "Titulaires de la QT", partTechIESSA: 1},
    {name: "Titulaires de la QTS et service non réorganisé", partTechIESSA:2},
    {name: "Titulaires de la QTS et service réorganisé", partTechIESSA: 3},
    {name: "Titulaires de la QTS + 10 ans et service non réorganisé", partTechIESSA: 4},
    {name: "Titulaires de la QTS + 10 ans et service réorganisé", partTechIESSA: 5},
];

export const partTechTSQualif = {
    2016: {
        1: 20,
        2: 236.43,
        3: 462.85,
        4: 701.96,
    },
    2018: {
        1: 20,
        2: 236.43,
        3: 462.85,
        4: 713.96,
    },
    2019: {
        1: 20,
        2: 236.43,
        3: 462.85,
        4: 725.96,
    },
    2023: {
        1: 20.70,
        2: 244.71,
        3: 479.05,
        4: 751.37,
    },
    2024: {
        1: 21.01,
        2: 248.38,
        3: 486.24,
        4: 762.64,
        5: 22.81,
        6: 269.61,
        7: 527.80,
        8: 827.83,
    },
}

export const partTechTSQualifLevel = [
    {name: "Détenteur de la 1re Qualification", partTechTSQualif: 1},
    {name: "Détenteur de la 2e Qualification < 4 ans", partTechTSQualif: 2},
    {name: "Détenteur de la 2e Qualification depuis ≥ 4 ans", partTechTSQualif: 3},
    {name: "Détenteur de la 2e Qualification depuis ≥ 4 ans ET sur emploi CTAC", partTechTSQualif: 4},
]


export const partTechTSHabil = {
    "01/01/2016": {
        1: 50.00,
        2: 124.44,
        3: 150.00,
        4: 162.45,
        5: 216.06,
        6: 250.00,
        7: 324.88,
        8: 360.55,
        9: 432.12,
        10: 485.22,
    },
    "01/01/2023": {
        1: 51.75,
        2: 128.80,
        3: 155.25,
        4: 168.14,
        5: 223.62,
        6: 258.75,
        7: 336.25,
        8: 373.17,
        9: 447.24,
        10: 502.20,
    },
    "01/01/2024": {
        1: 52.53,
        2: 130.73,
        3: 157.58,
        4: 170.66,
        5: 226.97,
        6: 262.97,
        7: 341.29,
        8: 378.77,
        9: 453.95,
        10: 509.73,
    },
    "01/07/2024": {
        1: 0,
        2: 80,
        3: 0,
        4: 0,
        5: 170.66,
        6: 130.73,
        7: 200,
        8: 130.73,
        9: 226.97,
        10: 130.73,
        11: 341.29,
        12: 350,
        13: 428.77,
        14: 453.95,
        15: 428.77,
        16: 509.73,
        17: 428.77,
        18: 600,
    },
    "01/01/2025": {
        1: 50,
        2: 80,
        3: 90,
        4: 150,
        5: 170.66,
        6: 180.73,
        7: 200,
        8: 220.73,
        9: 226.97,
        10: 280.73,
        11: 341.29,
        12: 350,
        13: 478.77,
        14: 453.95,
        15: 518.77,
        16: 509.73,
        17: 578.77,
        18: 600,
    },
}

export const partTechTSHabilLevel = {
    "1/1/2016": [
        {name: "Détenteur du niveau 1 de la licence de surveillance", partTechTSHabil: 1},
        {name: "Détenteur du certificat d’aptitude et AE dans les domaines énergie et climatisation", partTechTSHabil: 2},
        {name: "Habilitation contrôleur multi-système au CESNAC", partTechTSHabil: 2},
        {name: "Détenteur du niveau 2 de la licence de surveillance", partTechTSHabil: 3},
        {name: "Détenteur qualification de contrôles techniques d’aéronefs < 3 ans", partTechTSHabil: 4},
        {name: "Détenteur qualification de contrôles techniques d’aéronefs depuis < 3 ans  et exerçant en DSAC/N ou DSAC/NE", partTechTSHabil: 5},
        {name: "Détenteur du niveau 3 de la licence de surveillance", partTechTSHabil: 6},
        {name: "Détenteur qualification de contrôles techniques d’aéronefs ≥3 ans", partTechTSHabil: 7},
        {name: "Détenteur habilitation à rendre l’information de vol en CRNA (BTIV)", partTechTSHabil: 8},
        {name: "Détenteur habilitation à gérer l’aire de trafic à Roissy", partTechTSHabil: 8},
        {name: "Détenteur qualification de contrôles techniques d’aéronefs ≥ 3 ans  et exerçant en DSAC/N ou DSAC/NE", partTechTSHabil: 9},
        {name: "Détenteur habilitation coordonnateur en DCC", partTechTSHabil: 10},
    ],
    "1/7/2024": [
        { name: "License ANSO : niveau 1", partTechTSHabil: 1 },
        { name: "Licence DSAC : niveau 1", partTechTSHabil: 2 },
        { name: "License ANSO : niveau 2", partTechTSHabil: 3 },
        { name: "License ANSO : niveau 3", partTechTSHabil: 4 },
        { name: "CTE (hors dsac n/ne): Qualification < 3 ans (aéronef escale en France)", partTechTSHabil: 5 },
        { name: "AE énergie/clim ou habilitation contrôleurs multi-systèmes (CESNAC)", partTechTSHabil: 6 },
        { name: "Licence DSAC : niveau 2", partTechTSHabil: 7 },
        { name: "AE énergie/clim ou habilitation multi-systèmes (CESNAC) + ANSO niveau 2 ", partTechTSHabil: 8 },
        { name: "CTE DSAC-N/NE: Qualification < 3 ans (aéronef escale en France)", partTechTSHabil: 9 },
        { name: "AE énergie/clim ou habilitation multi-systèmes (CESNAC) + ANSO niveau 3", partTechTSHabil: 10 },
        { name: "CTE (hors dsac n/ne): Qualification > 3 ans (aéronef escale en France)", partTechTSHabil: 11 },
        { name: "Licence DSAC : niveau 3", partTechTSHabil: 12 },
        { name: "Habilitation SIV (en CRNA) ou VT (CDG)", partTechTSHabil: 13 },
        { name: "CTE DSAC-N/NE: Qualification > 3 ans (aéronef escale en France)", partTechTSHabil: 14 },
        { name: "Licence FISO niveau 2 + SIV ou licence AMS niveau 2 + VT (CDG)", partTechTSHabil: 15 },
        { name: "Qualification et AE coordonnateur en DCC", partTechTSHabil: 16 },
        { name: "Licence FISO niveau 3 + SIV ou licence AMS niveau 3 + VT (CDG)", partTechTSHabil: 17 },
        { name: "Licence DSAC : niveau 4", partTechTSHabil: 18 }

    ],
}


export const expe2016 = {
    "01/01/2016": {
        "iessa": {
            0:0,
            "A":40,
            "B":60,
            "service":100
        }
    },
    "01/01/2023": {
        "iessa": {
            0:0,
            "A":41.4,
            "B":62.1,
            "service":103.5
        }
    },
    "01/01/2024": {
        "iessa": {
            0:0,
            "A":43.06,
            "B":64.58,
            "service":107.64
        }
    },
    "01/07/2024": {
        "iessa": {
            0:0,
            "A":0,
            "B":64.58,
            "service":100,
            "PSS": 250,
        },
    },
    "01/01/2026": {
        "iessa": {
            0:0,
            "A":0,
            "B":0,
            "service":100,
            "PSS": 250,
        },
    },
}

export const expe2016IessaLevel = {
    "01/01/2016": {
        0:"Non réorganisé",
        "A":"Stade A",
        "B":"Stade B",
        "service":"Stade Projet de service"
    },
    "01/07/2024": {
        0:"Non réorganisé",
        "A":"Stade A",
        "B":"Stade B",
        "service":"Stade Projet de service",
        "PSS": "Projet de service spécifique",
    },
}


export const partLicence = {
    "1/1/2017": {
        1: 858.88,
        2: 1132.26,
        3: 1238.82,
        4: 1025.71,
        5: 1185.54,
        101:1060.26,
        102:1166.82,
        103:1166.82, //niveau 3 TS inexistant a cet epoque
    },
    "1/1/2019": {
        1: 865.88,
        2: 1132.26,
        3: 1238.82,
        4: 1025.71,
        5: 1185.54,
        101:1132.26,
        102:1238.82,
        103:1166.82, //niveau 3 TS inexistant a cet epoque
    },
    "1/1/2023": {
        1: 896.19,
        2: 1171.89,
        3: 1282.18,
        4: 1061.61,
        5: 1227.03,
        101:1171.89,
        102:1282.18,
        103:1282.18, //niveau 3 TS inexistant a cet epoque
    },
    "1/1/2024": {
        1: 932.04,
        2: 1258.42,
        3: 1376.85,
        4: 1077.53,
        5: 1276.11,
        101:1258.42,
        102:1376.85,
        103:1376.85, //niveau 3 TS inexistant a cet epoque
    },
    "1/7/2024": {
        1: 932.04,
        2: 1258.42,
        3: 1376.85,
        4: 377.53,
        5: 1626.11,
        6: 576.11,
        7: 1526.85,
    //Parts license TSEEAC : Ajouter + 100 a la valeur du niveau réel
        101:1258.42,
        102:1376.85,
        103:1526.85,
    },
    "1/1/2025": {
        1: 932.04,
        2: 1258.42,
        3: 1376.85,
        4: 377.53,
        5: 1976.11,
        6: 576.11,
        7: 1526.85,
        101:1258.42,
        102:1376.85,
        103:1526.85,

    },
}

export const partLicenceLevel = {
  "1/1/2016": [
    {name: "Suivant un PFU à Roissy ou Orly et détenant ou ayant détenu la mention LOC", partLicence: 1},
    {name: "ICNA Exerçant dans un groupe G", partLicence:2},
    {name: "ICNA Exerçant dans un groupe F", partLicence: 3},
    {name: "TSEEAC Exerçant dans un groupe G", partLicence:101},
    {name: "TSEEAC Exerçant dans un groupe F", partLicence: 102},
    {name: "PC exerçant dans un groupe D ou E avec toutes les MU, ICNA titulaire Suivant un PFU"+
           "à Roissy ou Orly et détenant ou ayant détenu la mention LOC", partLicence: 4},
    {name: "PC exerçant dans un groupe A, B ou C avec toutes les MU", partLicence: 5},
    ],
  "1/7/2024": [
    {name: "Pas de license", partLicence: 0}, //Ajout d'un niveau 0 fictif pour raison technique TSEEAC
    {name: "Suivant un PFU à Roissy ou Orly et détenant ou ayant détenu la mention LOC", partLicence: 1},
    {name: "ICNA Exerçant dans un terrain liste 11", partLicence:2},
    {name: "ICNA Exerçant dans un terrain liste 10", partLicence: 3},
    {name: "ICNA Exerçant dans un terrain liste 9", partLicence: 7},
    {name: "PC dans un organisme liste 7 ou 8 avec MU, ICNA titulaire suivant un PFU"+
           "à Roissy ou Orly et détenant ou ayant détenu la mention LOC", partLicence: 4},
    {name: "PC dans un organisme liste 4 à 6 avec MU", partLicence: 6},
    {name: "PC dans un organisme liste 1 à 3 avec MU", partLicence: 5},
    //Parts license TSEEAC : Ajouter + 100 a la valeur du niveau réel
    {name: "TSEEAC détenant et exerçant une mention d'unité totale, restreinte ou partielle d'un organisme classé dans la liste 11 ;", partLicence: 101},
    {name: "TSEEAC détenant et exerçant une mention d'unité totale, restreinte ou partielle d'un organisme classé dans la liste 10 ;", partLicence: 102},
    {name: "TSEEAC détenant et exerçant une mention d'unité totale, restreinte ou partielle d'un organisme classé dans la liste 9 ;", partLicence: 103},
  ]
};

export const complementPartLicence = {
    "1/1/2019": {
        "E": 845.81,
        "D": 909.54,
        "C": 1275.50,
        "B": 1333.71,
        "A1": 1747.94,
        "A2": 1808.68,
        "A3": 1897.06,
        "A4": 1957.80,
        "A5": 1990.95,
        "A6": 2025.51
    },
    "1/1/2023": {
        "E": 875.41,
        "D": 941.37,
        "C": 1320.14,
        "B": 1380.39,
        "A1": 1809.12,
        "A2": 1871.98,
        "A3": 1963.46,
        "A4": 2026.32,
        "A5": 2060.63,
        "A6": 2096.40,
    },
    "1/1/2024": {
        "E": 976.36,
        "D": 1051.15,
        "C": 1436.88,
        "B": 1502.45,
        "A1": 1969.09,
        "A2": 2037.51,
        "A3": 2137.08,
        "A4": 2205.50,
        "A5": 2242.84,
        "A6": 2281.78,
    },
    "1/7/2024": {
        "0": 0,
        "8": 1676.36,
        "7": 1751.15,
        "6": 2136.88,
        "5": 2136.88,
        "4": 2202.45,
        "3": 1969.09,
        "2": 2037.51,
        "1": 2281.78,
    },
};

export const majoComplementPartLicence = {
    "1/7/2024": {
        "11": 90,
        "10": 90,
        "9": 90,
        "8": 124.58,
        "7": 134.11,
        "6": 152.26,
        "5": 152.26,
        "4": 167.90,
        "3": 0,
        "2": 0,
        "1": 0,
    },
    "1/1/2025": {
        "11": 180,
        "10": 180,
        "9": 180,
        "8": 249.16,
        "7": 268.22,
        "6": 304.52,
        "5": 304.52,
        "4": 335.80,
        "3": 0,
        "2": 0,
        "1": 0,
    },
}

export const xpRH2016 = {
    2017: {
        "1":{
            groups: [["LFPG"], "default"],
            "0": 550,
            "1": 500,
        },
        "1bis": {
            groups: [
                ["LFEE", "LFLL"],
                ["LFRR", "LFBB", "LFMM"],
                ["LFPG"],
                "default"
            ],
            "0": 125,
            "1": 170,
            "2": 170,
            "3": 250,
        },
        "2": 255,
        "3": 255,
        "4": 500,
        "5": 255,
        "bonus1bis": {
            "0": 750,
            "1": 750,
            "2": 825,
            "3": 750,
        },
    },
    2023: {
        "1":{
            groups: [["LFPG"], "default"],
            "0": 569.25,
            "1": 517.5,
        },
        "1bis": {
            groups: [
                ["LFEE", "LFLL"],
                ["LFRR", "LFBB", "LFMM"],
                ["LFPG"],
                "default"
            ],
            "0": 129.38,
            "1": 175.95,
            "2": 175.95,
            "3": 258.75,
        },
        "2": 263.93,
        "3": 263.93,
        "4": 517.5,
        "5": 263.93,
        "bonus1bis": {
            "0": 776.25,
            "1": 776.25,
            "2": 853.88,
            "3": 776.25,
        },
    },
    2024: {
        "1":{
            groups: [["LFPG"], "default"],
            "0": 592.02,
            "1": 538.20,
        },
        "1bis": {
            groups: [
                ["LFPG"],
                "default"
            ],
            "0": 296.01,
            "1": 269.1,
        },
        "2": 274.49,
        "3": 274.49,
        "4": 538.20,
        "5": 274.49,
        "bonus1bis": {
            "0": 888.04,
            "1": 807.30,
        },
    },
}

/* 202546 RIST MAJO ATTRACT. GEO si pas 202547 */
export const attractiviteSortieEnac = {
  "1/1/2016": 0,
  "1/7/2024": 105.05 ,
}

/* 202547 RIST MAJO ATTRACT. CRNA pour IESSA */
export const fidelisationAthisReimsOrly = {
    "1/1/2016": 0,
    "1/7/2024": {
      icna: 300,
      iessa: 126.06
    }
}
