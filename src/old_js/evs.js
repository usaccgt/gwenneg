import { DataProviderFactory } from "./data.js"

const dp = DataProviderFactory.getInstance();

export function getLikelyFonction(corps, site, grade, echelon, encadrement) {

    let g = (encadrement == 1) ? new EncadrementGuesser(corps)
       : Guesser.fromCorps(corps)
    let list = g.guess(site, grade, echelon)
    if (list.some(a=>a?.niveau == undefined)) {
        list.filter(a=>a?.niveau == undefined).forEach(
            f => {
                console.warn(`Fonction ${f.key} : pas de niveau\n${f.toString()}`)
            }
        )
    }
    return list.filter(a=>a?.niveau);
};

/** Fonction factory,  utilise les clés du fichier YAML.
 * Certaines fonctions dépendent du site et doivent être appelé avec
 * un objet Site.
 *
 * ex:
 *   Fonctions.IESSAAffecté,
 *   Fonctions.PC(getSiteFromId('LFFF'))
 */
export const Fonctions = new Proxy({}, {
    get(target, prop, receiver) {
      let fromData = dp.fonctions[prop]
      return fromData == undefined ?
        undefined
        : new FonctionFromYaml(prop, fromData);
    },
});

const F = Fonctions

class Fonction {
    constructor(nom, desc, niveau) {
        this.label = nom,
        this.desc = desc;
        this.niveau = niveau;
    }
}

/* To handle site dependant Fonction, Fonction is callable.
 *
 * Explanations of super and bind logic in Adrien de Pierres's
 * stacokverflow answer https://stackoverflow.com/a/40878674/
 * from which code is adapted
 */
class FonctionFromYaml extends Function {
    constructor(key, fromData) {
        super('...args', 'return this.__self__.__call__(...args)')
        var self = this.bind(this)
        this.__self__ = self

        self.key = key;
        Object.assign(self, fromData)
        self.label = fromData?.label ?? key
        return self;
    }
    // Example `__call__` method.
    __call__(site) {
        this.niveau = FonctionFromYaml.niveauVariable(this.key,site);
        return this;
    }

    static niveauVariable(k, site) {
        let niveau;
        switch (k) {
            case "ExpertConf":
                niveau = 10;
                if (site.estSiegeSNA() || site.estGroupeA() || ['DTI', 'DO'].includes(site.oaci)) {
                    niveau = 11;
                }
                break;
            case "ExpertSenior":
                niveau = 11;
                if (site.estSiegeSNA() || site.estGroupeA() || ['DTI', 'DO'].includes(site.oaci)) {
                    niveau = 12;
                }
                break;
            case "PC":
                niveau = FonctionFromYaml.getEvsPC(site)
                break;
            case "PCDétaché":
                niveau = FonctionFromYaml.getEvsPC(site) + 1
                break;
            case "CDQ":
                niveau = FonctionFromYaml.getEvsCDQ(site)
                break;
            case "CDT":
                niveau = FonctionFromYaml.getEvsCDT(site)
                break;
            case "AssSub":
                niveau = FonctionFromYaml.niveauVariable("ChefSub",site) - 1;
                break;
            case "ChefSub":
                niveau = 11
                if (site.estSiegeSNA() || site.estGroupeA() || site.estSiegeDSAC() ||
                ['STAC', 'STAB', "BEA", "ENAC"].includes(site.oaci) || site.estSEAC() || site.estDACNC()) {
                    niveau = 12;
                }
                break;
            case "AdjPole":
                niveau = FonctionFromYaml.niveauVariable("ChefPole",site) - 1;
                break;
            case "ChefPole":
                niveau = 12
                if (site.estSiegeSNA() ||['ENAC', 'DTI', 'DO'].includes(site.oaci)) {
                    niveau = 13;
                }
                break;
            case "ChefCA":
                let niveaux = {
                    11: 5,
                    10: 5,
                    9: 5,
                    8: 7,
                    7: 8,
                }
                niveau = niveaux[site.liste]
        }
        return niveau;
    }

    static getEvsPC(site) {
        if (dp.isBeforeProtocole2023()) {
            const evsPC = {
                'E': 5,
                'D': 6,
                'C': 8,
                'B': 9,
                'A': 9
            }
            return evsPC[site.groupe[0]]
        } else {
            const evsPC = [
                undefined, //placeHolder rang 0
                9, // 1
                9, // 2
                9, // 3
                9, //4
                8, //5
                8, //6
                6, //7
                5, //8
            ];
            return evsPC[site.liste]
        }
    }
    static getEvsCDT(site) {
        if (dp.isBeforeProtocole2023()) {
            const evsCDT = {
                'E': 6,
                'D': 7,
                'C': 10,
                'B': 11,
                'A': 11
            }
            return evsCDT[site.groupe[0]];
        } else {
            const evsCDT = [
                undefined, //placeHolder rang 0
                11, // 1
                11, // 2
                11, // 3
                11, //4
                10, //5
                10, //6
                7, //7
                6, //8
            ]
            return evsCDT[site.liste]
        }
    }
    static getEvsCDQ(site) {
        if (dp.isBeforeProtocole2023()) {
            const evsCDQ = {
                'C': 9,
                'B': 10,
                'A': 10
            }
            return evsCDQ[site.groupe[0]];
        } else {
            const evsCDQ = [
                undefined, //placeHolder rang 0
                10, // 1
                10, // 2
                10, // 3
                10, //4
                9, //5
                9, //6
            ]
            return evsCDQ[site.liste];
        }
    }
}

class Guesser  {
    static fromCorps(corps) {
        switch (corps) {
            case 'iessa':
                return new IESSAGuesser();
            case 'icna':
                return new ICNAGuesser();
            case 'tseeac':
                return new TSEEACGuesser();
            default:
                return new Guesser();
        }
    }
    guess(site, grade, echelon) {
        console.warn('not implemented, should be overloaded');
        return [];
    }
}

class IESSAGuesser extends Guesser {
    guess(site, grade, echelon) {
        if (site.oaci == 'ENAC') {
            return this.guessENAC(site, grade, echelon);
        } else if (site.oaci == 'DTI') {
            return this.guessDTI(site, grade, echelon);
        }
        let likely;
        switch (grade) {
            case "eleve":
                return [];
            case "stagiaire":
                return [F.IESSAAffecté, F.IESSAExamProFinStage];
            case "normal":
                likely = [
                    F.IESSATitu,
                    F.DMS
                ];
                if (site.estSiegeSNA()) {
                    likely.push(F.STM);
                }
                if (echelon >= 2) {
                    likely.push(
                        F.QTS,
                        F.RSO,
                        F.GRT,
                    )
                }
                if (echelon >= 3) {
                    likely.push(
                        F.ExpertConf(site),
                    );
                }
                return likely;
            case "divisionnaire":
                likely = [
                    F.QTS,
                    F.RSO,
                    F.GRT,
                    F.IATSEP,
                    F.ExpertConf(site),
                    F.GRTS
                ]
                if (echelon >= 3) {
                    likely.push(
                        F.ExpertSenior(site),
                    );
                }
                if (echelon >= 4) {
                    likely.push(
                        F.IL
                    );
                }
                return likely;
        }
        return [];
    }
    guessENAC(site, grade, echelon) {
        let likely;
        switch (grade) {
            case "eleve":
                return [];
            case "stagiaire":
                return [
                    F.Promo,
                    IESSAAffecté];
            case "normal":
                likely = [
                    F.IESSATitu,
                    F.IESSADétaché1236
                ];
                if (echelon >= 2) {
                    likely.push(
                        F.QTS,
                        F.IESSAInstructeurENAC,
                    )
                }
                if (echelon >= 3) {
                    likely.push(
                        F.EnsConf,
                    );
                }
                return likely;
            case "divisionnaire":
                likely = [
                    F.QTS,
                    F.IESSAInstructeurENAC,
                    F.EnsConf,
                ]
                if (echelon >= 3) {
                    likely.push(
                        F.EnsSenior,
                    );
                }
                if (echelon >= 4) {
                    likely.push(
                        F.IE,
                    );
                }
                return likely;
            default:
                return [F.EnsConf, F.EnsSenior]
        }
    }
    guessDTI(site, grade, echelon) {
        let likely = [];
        switch (grade) {
            case "eleve":
                return [];
            case "stagiaire":
                return [F.IESSAAffecté, F.IESSAExamProFinStage];
            case "normal":
                likely = [
                    F.IESSATitu,
                    F.IESSADétaché1236,
                    F.Spécialiste,
                ];
                if (echelon >= 2) {
                    likely.push(
                        F.QTS,
                        F.SpécialisteConf,
                        F.ExpertDTI,
                    )
                }
                if (echelon >= 3) {
                    likely.push(
                        F.ExpertConf(site),
                    );
                }
            case "divisionnaire":
                likely = [
                    F.QTS,
                    F.ExpertDTI,
                    F.ExpertConf(site),
                ];
                if (echelon >= 3) {
                    likely.shift(); // remove QTS
                    likely.push(
                        F.ExpertSenior(site),
                    );
                }
                break;
            default:
                likely = [
                    F.ExpertConf(site),
                    F.ExpertSenior(site),
                ]
        }
        return likely;
    }
}
class ICNAGuesser extends Guesser {
    getDescTypeOrganisme(site) {
        if (dp.isBeforeProtocole2023()) {
            return `groupe ${site.groupe[0]}`
        } else {
            return `liste ${site.liste}`
        }
    }
    getLikelyPC(site) {
        let PC = F.PC(site)
        PC.desc = `PC des organismes de ${this.getDescTypeOrganisme(site)}`
        let détaché = F.PCDétaché(site)
        détaché.desc = `détaché des organismes de ${this.getDescTypeOrganisme(site)}`
        return [ PC, détaché];
    }
    guess(site, grade, echelon) {
        let likely;
        if (site.oaci == 'ENAC') {
            return this.guessENAC(site, grade, echelon);
        }
        switch (grade) {
            case "eleve":
                return [];
            case "stagiaire":
            case "normal":
                return [F.ICNAAffecté, F.ICNA1MI, F.ICNA2MI, F.ICNA3MI];
            case "divisionnaire":
            case "en chef":
                likely = this.getLikelyPC(site);
                if (echelon >= 3 || grade == "en chef") {
                    if (site.estCRNA()){
                        likely.push(F.CDE, F.ACDS, F.CDS);
                    } else {
                        if (
                            (dp.isBeforeProtocole2023() && site.groupe[0] <= "C")
                            || parseInt(site.liste, 10) <= 6
                         ) {
                            likely.push(F.CDQ(site))
                        }
                        likely.push(F.CDT(site))
                    }
                    if (site.oaci == "LFPG") {
                        likely.push(F.CDA)
                    }
                }
                return likely;
        }
        return [];
    }

    guessENAC(site, grade, echelon) {
        switch (grade) {
            case "élève":
            case "stagiaire":
                return [];
            default:
                return [
                    F.ICA,
                    F.RéférentICA,
                    F.EnsConf,
                    F.EnsSenior,
                    F.IE,
                ]
        }
    }
}

class TSEEACGuesser extends Guesser {
    guess(site, grade, echelon) {
        let likely = [];
        if (["eleve", "stagiaire"].includes(grade)) {
            return [];
        }

        //A améliorer dessous : seulement RTAC et pas tout détaché pour ATC
        if (site.estTerrainTS() && ["normal", "exceptionnel", "principal", "détaché"].includes(grade)) {
            likely.push(F.AtcAD);
            likely.push(F.ChefCA(site));
        }

        //Operationnels
        if (site.oaci == "LFPG") {
            likely.push(F.VT);
            likely.push(F.CdqVT);
        }
        if (site.oaci == "LFPO") {
            likely.push(F.PCNA);
        }
        if (site.estCRNA()) {
            likely.push(F.BTIV);
            likely.push(F.ChefBTIV);
        }

        if (["normal", "exceptionnel", "principal"].includes(grade)) {
            likely.push(...[
                F.SpécFormation,
                F.Spécialiste,
                F.SpécConfirmé,
            ]);
        }

        if (site.estSiegeDSAC()) {
            likely.push(F.ISdéb1, F.ISdéb2, F.IS);
        }

        if (["exceptionnel", "principal", "détaché"].includes(grade)) {
            if (site.estSiegeSNA() || site.estGroupeA() || site.estSiegeDSAC() ||
            ['STAC', 'STAB', "BEA", "ENAC"].includes(site.oaci) || site.estSEAC() || site.estDACNC()) {
                //SEAC, DAC/NC
                // DO/EC -> siegeDSAC avec FARM et LFFF, BEA dans LFPB
                likely.push(F.AssSub(site))
                likely.push(F.ExpertConf(site))
                likely.push(F.ExpertSenior(site))
            }
            else if(site.oaci == "DTI") {
                likely.push(F.ExpertDTI)
                likely.push(F.ExpertConf(site))
                likely.push(F.ExpertSenior(site))
            } else {
                likely.push(F.AssSub(site))
                // expert conf et/ou senior dans les petits centres ?
            }
        }

        //Bordeaux et ENAC
        if(site.oaci == "ENAC") {
            likely.push(F.OpeSimuENAC);
        }
        if (["ENAC", "CESNAC"].includes(site.oaci)) {
            likely.push(F.CtrlSystForm);
            likely.push(F.CtrlSyst);
            likely.push(F.SupSys);
        }
        if (site.oaci == "CESNAC") {
            likely.push(F.CMS);
            // likely.push(new Fonction("BNI", "", 9));
            likely.push(F.BNIA);
        }

        if (["FMEE", "TFRR", "TFFF", "NTAA", "NWWW"].includes(site.oaci)) {
            likely.push(F.BRIA);
        }


        return likely;
    }
}

class EncadrementGuesser extends Guesser {
    constructor(corps) {
        super();
        this.corps = corps;
    }
    guess(site, grade, echelon) {
        if (site.oaci == "DTI") {
            return this.guessDTI(site, grade, echelon);
        }
        let likely = [];
        likely.push(
            F.AssSub(site),
            F.ChefSub(site),
            F.AdjPole(site),
            F.ChefPole(site),
        )
        if (site.estSiegeSNA() || site.estCRNA()) {
            likely.push(
                F.AdjService,
                F.ChefService
            )
        } else if (site.groupe != undefined) {
            likely.push(
                F.ChefCA(site),
            )
        }
        if(site.estSiegeSNA() || site.estCRNA() || site.oaci == "CESNAC" || site.oaci == "SIA") {
            likely.push(
                F.ChefOrganisme
            )
        }
        if(site.estSiegeSNA() || site.estCRNA() || site.estSiegeDSAC() || ["ENAC", "STAC", "BEA", "SNIA"].includes(site.oaci)) {
            likely.push(
                F.ChefDepartement
            )
        }
        if(site.estSiegeSNA() || site.estCRNA() || ["ENAC, SNIA"].includes(site.oaci)) {
            likely.push(
                F.AdjChefDepartement
            )
        }
        if(site.oaci == "DNUM") {
            likely.push(
                F.ChefDomaineDNUM,
                F.AdjChefDomaineDNUM
            )
        }
        if((site.estSiegeSNA() && site.groupe > 3) ||  site.estSiegeDSAC()) {
            likely.push(
                F.ChefDivDSACListe3
            )
        }
        if((site.estSiegeSNA() && site.groupe <= 3)) {
            likely.push(
                F.ChefDiv
            )
        }
        if(site.oaci == "LFFF") {
            likely.push(F.ChefDivSpecialDSACN);
        }
        return likely;
    }
    guessDTI(site, grade, echelon) {
        return [
            F.AdjPole(site),
            F.ChefPole(site),
            F.AdjPoleMajeur,
            F.ChefPoleMajeur,
            F.AdjDomaineDTI,
            F.ChefDomaineDTI,
        ]
    }
}
