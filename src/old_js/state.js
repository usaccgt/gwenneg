/**
 * Copyright (C) 2022-2023 USACcgt
 * License AGPL 3.0
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program. If not, see <https://www.gnu.org/licenses/>
 *
 */

import { Observer } from "./utils.js";

/* Fire message named according property (corps, grade, evs...)*/
export class InputState extends Observer {
    static validProps = [
        "corps",
        "grade",
        "evs",
        "region",
        "famille",
        "pcs",
        "rembt",
        "indem_csg",
        "tauxIR",
        "validity",
        "echelon",
        "nbi",
        "peq",
        "reorg",
        "nuit",
        "rma",
        "majPFRistCDG",
        "detachement",
        "licence",
        "groupe",
        "affect",
        "majDOMTOM",
        "part3",
        "partTechTSHabil",
        "TSEtape",
        "psc",
        "xpRH",
        "xpRHOption",
        "xpRHGroupe",
        "xpRHCyclesRH"
    ]

    constructor() {
        super();
        this.properties = {}
    }

    //special cases with default value
    get corps() {
        return this.properties.corps||"ieeac";
    }


    set(name,value) {
        console.debug(`InputState.set | ${name},${value}`)
        if (this.constructor.validProps.includes(name)) {
            if (this.properties[name] !== value) {
                if ((typeof value == "string" && value.length == 0) || value == null) {
                    this.unset(name);
                } else {
                    this.properties[name] = value;
                    this.fire(name, value)
                }
            }
        }
        else {
            throw new Error(`Invalid property : ${name}`)
        }
    }
    unset(name) {
        console.debug(`InputState.unset | ${name}`)
        if (this.constructor.validProps.includes(name)) {
            if (this.properties[name]) {
                delete this.properties[name];
                this.fire(name, null)
            }
        }
        else {
            throw new Error(`Invalid property : ${name}`)
        }
    }
}
