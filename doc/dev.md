# Gwenneg - Documentation développeur
***Simulateur de salaire fonction publique par [l'USACcgt](https://www.usac-cgt.org/)***

[[_TOC_]]

## Vos Messages de commit (Changelog auto)

Le dépot fait appel à l'API de gitlab afin de générer automatiquement le changelog technique.

Le Changelog est construit à partir des messages de commit. Il y a deux situations lorsque vous créez un commit :

- Le commit ne doit pas figurer dans le changelog technique : vous n'avez rien de spécial à faire.
- Le commit mérite de figurer dans le changelog technique : vous devez formatter votre message de commit de la manière suivante :

```
<Titre du Commit>

<Détails éventuels>

Changelog: XXXX
```

Le titre du commit (id: La 1ère ligne) sera repris dans le changelog sous la catégorie XXXX.

Les catégories suivantes sont supportés :

- Pour les fonctionnalités du programme (coté "back") :
  - **added** : Ajouts
  - **fixed** : Corrections
  - **changed** : Modifications
  - **deprecated** : Dépréciations
  - **removed** : Retraits
  - **security** : Sécurités
- Concernant les changements apportés à l'interface utilisateur :
  - **ui**
- Autres :
  - **archi** : "Modifications dans l'architecture du projet"
  - **other** : Autres

La liste est discutable et amendable. Ca se passe dans le fichier `.gitlab/changelog_config.yml` (afin d'afficher les catégories en français)

## Publier une nouvelle version

La CI se charge d'une bonne partie du travail, vous devez cependant initialiser la publication avec un commit qui doit contenir les modifications suivantes :

- Dans le fichier package.json, changer la variable version (ligne 3)
- Modifier la variable GWENNEG_VERSION dans le fichier GWENNEG_VERSION.yml
- Mettre à jour le changelog utilisateur dans le fichier ABOUT.md

Pour lancer le processus, le message de commit devra contenir `[publish]`

3 étapes se dérouleront automatiquement : 

1. Lancements des tests & mise à jour du fichier CHANGELOG.md *(un nouveau commit est crée dont le message contiendra `[rls]`)*
1. Création de la release gitlab et remplissage avec les notes de version. *Celà crée également un tag git*
1. Compilation et publication des versions tests et opérationnels.

Celà peut prendre plusieurs minutes et les tests peuvent échouer. Ne lancez pas une publication juste avant de partir !
