# Gwenneg - Commandes NPM & fonctionnement CI
***Simulateur de salaire fonction publique par [l'USACcgt](https://www.usac-cgt.org/)***

## Commandes NPM

Le Simulateur repose sur Vuejs pour le coté front-end et un mélange de javascript et de typescript pour le coté back-end, notamment pour le moteur de calcul de la paye. Le tout est compilé par Vite.

Les commandes npm sont les commandes classiques de Vite plus quelques autres dans le bnut de supporter Gwenneg-ide (qui réclame des ports particuliers), les tests et le build vers les gitlab pages du dépot mère.

### Commandes classiques

- Mode developpement : `npm run dev`
- Compilation (version opé) : `npm run build`
- Compilation (version test) : `npm run build:vtest`
- Lancement de tous les tests (dont les e2e cypress) : `npm run test`
- Il existe aussi `npm run lint` et `npm run format`

### Adaptation pour GwennegIDE

[GwennegIDE](https://gitlab.com/JiCambe/gwenneg-ide) repose sur une VM qui encapsule un VSCode (web) préconfiguré. Afin de ne pas rentrer en conflit avec un autre vite qui tournerait sur la machine hote, il utilise le port 8080 pour le serveur de dev.
La commande configuré pour celà est `npm run dev-ide`

### Adaptation pour les gitlab pages

Lors de la compilation, Vite a besoin de connaitre l'éventuel sous répertoire où tournera l'app web. Or, dans le dépot principal, les gitlab pages seront disponible à l'adresse : https://usaccgt.gitlab.io/gwenneg/
Pour que les pages fonctionnent, nous utilisons donc un fichier spécifique de configuration pour vite : vite.config.gwenned.repo.ts et la compilation pour ses pages sera lancé (par la ci) par la commande `npm run build:vpages:repomere`

## Fonctionnement de la CI

Les principes de la CI actuel sont décrits ci-dessous. L'ensemble du workflow est largement améliorable. Il me semble notamment qu'on peut passer les tests plusieurs fois pour les même modifications. (idem pour les uploads)
Cependant, j'ai fait au mieux avec le peu de temps que j'avais en tentant d'assurer le coup.

La CI est composé des Stages suivants : 

- **prepare** : installation de vue & MAJ des données sources (datas/csv)
- **test** : Lance les tests
- **build** : Compilation des différentes version du simulateur
- **deploy** : Upload vers les différents sites
- **release** : Création d'une release d'après un TAG

Seuls les stages et les jobs appropriés sont crées et lancés lors d'un évènement.

Plus bas, nous allons détailler chaque évènement qui lance un pipeline.

**Les pipelines NE SERONT PAS CREE si le message de commit contient [no-ci].**

### Une requète de Fusion (Merge Request)

Les stages *prepare* et *test* sont lancés afin d'être sur que la proposition de modification n'a pas cassé un test.

### Commit dans la branche principale du dépot principal

Les stages *prepare* et *test* sont lancés.

Les versions de tests (gitlab pages et https://test-simu.usac-cgt.org/) sont compilés (par *build*) et mis à jour (par *deploy*)

Pour la construction de la version gitlab pages, le configuration spécifique au dépot principal est utilisé

### Commit dans un fork A CONDITION que le message de commit contiennent [with-ci]

Les stages *prepare* et *test* sont lancés.

Les stages *build* et *deploy* compile et upload une gitlab page accessibble via le menu `Déploiement > Pages`

### Création d'une étiquette (tag) de la forme (v)X.Y.Z

Lors de la création d'une étiquette genre v1.0.1 ou 1.2.0, Les stages *prepare* et *test* sont lancés.

Si le test se déroule correctement, une release est crée par le stage *release*.

Puis *build* et *deploy* sont lancés afin de compiler et mettre à jour l'ensemble des versions (ope + test).
(car je ne suis pas sur que si on tag un commit dont le)


