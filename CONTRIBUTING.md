# En cas de problèmes

Gwenneg est un simulateur de salaire édité par [l'USACcgt](https://www.usac-cgt.org/). Il se peut que vous constatiez une différence entre le résultat du simulateur et votre feuille de paye. Il peut y avoir plusieurs explications :

- Votre situation a récemment changé ou il vous manque une prime nouvellement crée/modifié. sachez qu'il peut s'écouler quelques mois avant que les modifications soient effective sur votre feuille de paye (les sommes dûes ou trop perçues seront régularisées à ce moment là),
- Il y a une erreur avec votre feuille de paye,
- Il y a une erreur dans le simulateur.

N'hésitez pas à contacter vos représentants USACcgt.

Vous trouverez plus bas la liste des principaux problèmes connus du simulateur, ainsi que les méthodes pour nous signaler des erreurs du simulateur. Merci par avance pour votre participation.

### Principaux problèmes connus

- **Protocole 2024** : L'ensemble des changements (et des nouvelles primes) protocolaire ne sont pas encore tous disponible. Nous sommes en train de travailler dessus.
- **Outre-mer** : les sur-rémunérations OM de Mayotte, de la Polynésie, de la Nouvelle-Calédonie et de Wallis n'apparaissent pas encore dans le résultat. nous sommes à la recherche de l'ensemble des informations nécéssaires. [Cette page du wiki peut vous aider](https://usac-cgt.org/wiki/index.php?title=Majoration_et_indexation_de_la_r%C3%A9mun%C3%A9ration_en_outre-mer)
- Il peut-être difficile de retrouver sa fonction. (c'est plutôt de la faute à la liste absconse de l'admin...)
- Il manque la gestion des titres-restaurants

Vous pouvez trouvez [la liste complète ici](https://gitlab.com/usaccgt/gwenneg/-/issues)

*Vous devez rentrer manuellement la compensation CSG, nous n'avons pas de moyen fiable de calculer cette somme.*

### Signaler des erreurs

En cas d'erreur sur le simulateur, nous vous remercions de nous le signaler. Voici les différentes manières de le faire :

#### Utiliser le bouton dédié

Dans le mode avancé, vous avez un bouton "envoyer un rapport de bug". Ce bouton génère une trame de mail qu'il vous suffit de compléter puis d'envoyer.

Utiliser ce bouton nous permet de connaitre précisément les paramètres que vous avez rentré. N'hésitez pas à l'utiliser en priorité.

![](doc/img/declarer_bug_1.jpg)
![](doc/img/declarer_bug_2.jpg)

#### Nous envoyer un mail

Envoyer un mail à l'adresse suivante : [contact-simulateur@usac-cgt.org](mailto:contact-simulateur@usac-cgt.org)

#### Utiliser les tickets gitlab

Si vous avez un compte gitlab, vous pouvez utiliser directement [le système de ticket](https://gitlab.com/usaccgt/gwenneg/-/issues).
Pas d'inquiétude si ça vous semble compliqué. Envoyez simplement un mail à la place.
