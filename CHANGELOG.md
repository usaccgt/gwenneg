# Liste des changements par version

Voici les changemements important pour chaque version du point de vue technique.

## 0.12.0 (24/02/2025)

### Ajouts (7 changements)

- [feat: ajout indemnité d'astreinte](https://gitlab.com/usaccgt/gwenneg/-/commit/b53cc696b7c50dc964192dc79f869886febd2a17)
- [feat(ui): création composant Foldable et RemoveButton](https://gitlab.com/usaccgt/gwenneg/-/commit/44e54149c77afb3c5727ac36a1afd23e3f6092c7)
- [feat(function) prime DO, maj liste fonction, ajout ExpertForm](https://gitlab.com/usaccgt/gwenneg/-/commit/b5b1d2990074eb93709fd12de2eb0718a0eec650)
- [Ajout de la prime 2546 (Attractivité sortie école) pour les IEEAC](https://gitlab.com/usaccgt/gwenneg/-/commit/7340fddf0e0b056f92801471bdcf8740388bc817) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/17))
- [Ajout de la prime 2546 (attractivité sortie école) pour les TSEEAC](https://gitlab.com/usaccgt/gwenneg/-/commit/68d3dabcc99c37c21421829ed59f503670e0baa8) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/17))
- [Ajout des primes 2546 (sortie enac) et 2547 (fidélisation Ath, Rms, Ory) pour les ICNA](https://gitlab.com/usaccgt/gwenneg/-/commit/dab7df12154c8ff2d43311b9d461e4afe39951bc) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/17))
- [feat: ajout input CTACable pour part tech TSEEAC](https://gitlab.com/usaccgt/gwenneg/-/commit/0af83265b5a153abb584e23beaf20b42a47ab7a6)

### Corrections (4 changements)

- [fix(ui): sépare encadrement et showEncadrement](https://gitlab.com/usaccgt/gwenneg/-/commit/098e0b7d17eee3e3458302bc3e05d6cfe40e8ea2)
- [Correction sur le forfait télétravail : Il a un montant max](https://gitlab.com/usaccgt/gwenneg/-/commit/e3b2f25a12c33db33555a07a5ab7dda82a54cfd5) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/18))
- [ticket #23 : Ajout de "avant impôts" à "Salaire net à payer"](https://gitlab.com/usaccgt/gwenneg/-/commit/fa7c00fd6c72597e867964d7d61e558276780308) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/18))
- [Correction : La prime 2546 attractivité sortie école n'a pas à être versé...](https://gitlab.com/usaccgt/gwenneg/-/commit/5826ec8f0964ffe331555cd581f4ffd87d654c93) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/17))

### Modifications (1 changement)

- [build: include csv import in vite build](https://gitlab.com/usaccgt/gwenneg/-/commit/604e015bd34fa9ace96716ce5aa43078987357d6)

## 0.11.1 (05/02/2025)

Pas de changements.

## 0.11.0 (05/02/2025)

Pas de changements.

## 0.10.1 (20/01/2025)

Pas de changements.

## 0.10.0 (20/01/2025)

### Ajouts (2 changements)

- [Ajout de la prime sortie école pour les IESSA](https://gitlab.com/usaccgt/gwenneg/-/commit/df3d5fad930ee0d090bd3966e74f3b26ef6aaeab) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/13))
- [Ajout des nouveaux code wiki de la page majoration géographique](https://gitlab.com/usaccgt/gwenneg/-/commit/c2f26fdf205a5d8612bb92420caa0d36fd0c116a) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/13))

### Corrections (1 changement)

- [fix: Le nom complet de Melun est Melun-Villaroche](https://gitlab.com/usaccgt/gwenneg/-/commit/42fb5df8e2b307cdfa588028eb1ea6d06654c1f0) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/13))

### Modifications (1 changement)

- [(Front) Modification du champ sitePicker en ajoutant le code OACI du site aporès le nom du centre](https://gitlab.com/usaccgt/gwenneg/-/commit/15264eaa304493b0939eeb36d726deb363d6880f) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/13))

### Modifications dans l'architecture du projet (2 changements)

- [[CI] Split du Fichier de conf](https://gitlab.com/usaccgt/gwenneg/-/commit/6bbb91f7957dcc03e1bd9b8f05d6586ba2d16dc0) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/16))
- [[CI] Génération automatique du changelog technique](https://gitlab.com/usaccgt/gwenneg/-/commit/cd143034c1afdb21d0f1d6a23702ea0491a0ede6) ([merge request](https://gitlab.com/usaccgt/gwenneg/-/merge_requests/16))

### CI (1 changement)

- [Remplacement des GitLabPages du repo mère par une redirection vers le simu officiel](https://gitlab.com/usaccgt/gwenneg/-/commit/82149fa4105aedf4dc32b17d8e457b1ae30f0e4a)
