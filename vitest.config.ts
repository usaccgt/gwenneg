import { fileURLToPath } from 'node:url'
import { mergeConfig, defineConfig, configDefaults } from 'vitest/config'
import viteConfig from './vite.config'

export default defineConfig(configEnv => mergeConfig(
  viteConfig(configEnv),
  defineConfig({
    test: {
      environment: 'jsdom',
      exclude: [
        ...configDefaults.exclude,
        'tests/cypress/**',
        'tests/core/**',
        '.gitlab-ci-local/**'
      ],
      root: fileURLToPath(new URL('./', import.meta.url))
    }
  })
))
