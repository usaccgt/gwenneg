import { fileURLToPath, URL } from 'node:url'

import gitdescribe from 'git-describe';
const { gitDescribeSync } = gitdescribe;

import { Plugin, defineConfig, HmrContext } from 'vite'
import vue from '@vitejs/plugin-vue'

import { Mode, plugin as mdPlugin } from "vite-plugin-markdown";
import { viteStaticCopy } from 'vite-plugin-static-copy';

import { importCsv } from './bin/csv/import';
import { importYaml } from './bin/yaml/import';
import { writeSitemap } from './bin/sitemap.js';

const date = new Date();
const locale = 'fr-FR';
const options: Intl.DateTimeFormatOptions = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric'
};
const formatter = new Intl.DateTimeFormat(locale, options);

const desc = gitDescribeSync();

/* black magic from: https://www.reddit.com/r/vuejs/comments/1avsjzf/comment/l2clopo/
 * to provide source map in serve mode */
import fs from 'node:fs';
import path from 'node:path';
const __dirname = import.meta.dirname

function fixSourceMaps() {
  let currentInterval: NodeJS.Timeout | undefined;
  return {
    name: 'fix-source-map',
    enforce: 'post',
    transform: function (source: any) {
      if (currentInterval) {
        return;
      }
      currentInterval = setInterval(function () {
        const nodeModulesPath = path.join(__dirname, 'node_modules', '.vite', 'deps');
        if (fs.existsSync(nodeModulesPath)) {
          clearInterval(currentInterval);
          currentInterval = undefined;
          const files = fs.readdirSync(nodeModulesPath);
          files.forEach(function (file) {
            const mapFile = file + '.map';
            const mapPath = path.join(nodeModulesPath, mapFile);
            if (fs.existsSync(mapPath)) {
              let mapData = JSON.parse(fs.readFileSync(mapPath, 'utf8'));
              if (!mapData.sources || mapData.sources.length == 0) {
                mapData.sources = [path.relative(mapPath, path.join(nodeModulesPath, file))];
                fs.writeFileSync(mapPath, JSON.stringify(mapData), 'utf8');
              }
            }
          });
        }
      }, 100);
      return source;
    }
  }
}
/* end of black magic */

function stripDevFile () {
  return {
    name: 'strip-dev-file',
    renderStart (outputOptions: any, inputOptions: any) {
      const outDir = outputOptions.dir;
      fs.unlinkSync(path.resolve(outDir,'rss.xml'));
    }
  }
}

// https://vite.dev/config/
export default defineConfig(({ command }) => {
  const config = {
    plugins: [
      {
        name: 'pre-build-script',
        buildStart() {
          importCsv();
          importYaml();
          if (command === 'build') {
            writeSitemap('public/sitemap.xml');
          }
        },
        handleHotUpdate(context: HmrContext) {
          if (context.file.match(/datas\/yaml\/.*yml/)) {
            importYaml();
          } else if (context.file.match(/datas\/csv\/.*csv/)) {
            importCsv();
          } else if (context.file.match(/public\/css\/.*css/)) {
            context.server.ws.send({ type: 'full-reload' })
          }
        },
      },
      vue(),
      mdPlugin({ mode: [Mode.HTML] }),
      viteStaticCopy({
        targets: [
          {
            src: 'doc/img',
            dest: 'doc'
          }
        ]
      }),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src/frontend', import.meta.url))
      }
    },
    // Pour éviter les longues images incorporé dans l'html via un base64
    // Voir le fix des icones de lefset : https://cescobaz.com/2023/06/14/setup-leaflet-with-svelte-and-vite/
    build: {
      assetsInlineLimit: 50, //Parce que si ça fait moins de 50 octets, on doit pouvoir y inclure
      rollupOptions: {
        output: {
          manualChunks: {
            leaflet: ['leaflet']
          }
        },
      },
    },
    define: {
      __BUILD_DATE__: JSON.stringify(formatter.format(date)),
      __VUE_APP_GIT_HASH__: JSON.stringify(desc.hash),
      __VUE_APP_SEMVER_STRING__: JSON.stringify(desc.semverString),
      __IS_DEV__: command == "serve",
    },
  };
  if (command == "serve") {
    config.plugins.push(fixSourceMaps() as Plugin);
  } else {
    config.plugins.push(stripDevFile() as Plugin)
  }
  return config;
});
